package com.exam.util;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;

import java.util.HashMap;

/**
 * 阿里云短信--工具
 */
public class SmsUtil {

    private static final String endpoint = "dysmsapi.aliyuncs.com";
    private static final String AccessKeyId = "LTAI4GL9Z59YPf3HDZLq3bVp";
    private static final String AccessKeySecret = "QRJdvpS361ETf6ZJwd5dNanBq769Ve";
    private static final String SignName = "小鹿考试系统";
    private static final String TemplateCode="SMS_206737805";


    /**
     * 发送验证码
     * @param phone 手机号
     * @param code 验证码
     */
   public static boolean Send(String phone,String code){
       DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", AccessKeyId, AccessKeySecret);
       IAcsClient client = new DefaultAcsClient(profile);

       CommonRequest request = new CommonRequest();
       request.setSysMethod(MethodType.POST);
       request.setSysDomain(endpoint);
       request.setSysVersion("2017-05-25");
       request.setSysAction("SendSms");
       request.putQueryParameter("RegionId", "cn-hangzhou");
       //自定义的参数(手机号,验证码,签名,模板!)
       request.putQueryParameter("PhoneNumbers", phone);
       request.putQueryParameter("SignName", SignName);//签名（要和之前申请的保持一致）
       request.putQueryParameter("TemplateCode", TemplateCode);//模板管理里的模板CODE
       //构建一个短信的验证码
       HashMap<String, Object> map = new HashMap();
       map.put ("code",code);
       request.putQueryParameter("TemplateParam", JSONObject.toJSONString(map));
       try {
           CommonResponse response = client.getCommonResponse(request);
           System.out.println(response.getData());
           return true;
       } catch (ServerException e) {
           e.printStackTrace();
       } catch (ClientException e) {
           e.printStackTrace();
       }
       return false;
   }




}
