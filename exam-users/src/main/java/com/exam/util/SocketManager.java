package com.exam.util;

import javax.websocket.Session;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * websocket帮助类
 */
public class SocketManager {
    private Map<String, Session> mapSession = new ConcurrentHashMap();

    private volatile static SocketManager socketManager;

    public Map<String, Session> getMapSession() {
        return mapSession;
    }

    public void setMapSession(Map<String, Session> mapSession) {
        this.mapSession = mapSession;
    }

    public SocketManager() {
    }

    public static SocketManager getInstance(){
        if (socketManager==null){
            synchronized (SocketManager.class){

                if (socketManager==null){
                    socketManager = new SocketManager();
                }
            }
        }
        return socketManager;
    }

}

