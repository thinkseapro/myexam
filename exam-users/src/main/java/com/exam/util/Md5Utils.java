package com.exam.util;


import org.apache.shiro.crypto.hash.SimpleHash;
import org.springframework.util.DigestUtils;

/**
 * MD5加密，不可逆
 */
public class Md5Utils {
    /**
     * md5加密
     *
     * @param value
     * @return
     */
    public static String md5Hex(String value) {
        return DigestUtils.md5DigestAsHex(value.getBytes());
    }


    /**
     * 3次md5加密
     *
     * @param value
     * @return
     */
    public static String md5Hex3(String value) {
        for (int i = 0; i < 3; i++) {
            value = DigestUtils.md5DigestAsHex(value.getBytes());
        }
        return value;
    }

    /**
     * 密码加密(shiro)
     *
     * @param password 密码(前台参入过来的密码)
     * @param salt     盐 数据库中的盐
     * @return
     */
    public static String md5Encryption(String password, String salt) {
        String algorithmName = "MD5";//加密算法 md5
        int hashIterations = 1024;//加密次数 1024
        Object obj = new SimpleHash(algorithmName, password, salt, hashIterations);
        return obj + "";
    }





    public static void main(String[] args) {

   //     System.out.println(md5Hex3("123") + "admin".length());
//        7363a0d0604902af7b70b271a0b964805
//对应盐:  400aaf9e7ae54a76a0a018a9a842b1ae
  //      System.out.println(md5Encryption("7363a0d0604902af7b70b271a0b964805", "400aaf9e7ae54a76a0a018a9a842b1ae"));

        String password="123456";
        md5Hex3(password);
        password+=2;
        System.out.println(password);
    }
}
