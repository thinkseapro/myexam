package com.exam.util;

import com.exam.pojo.email.EmailParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

@Component
public class EmailUtils {

    @Autowired
    private JavaMailSenderImpl mailSender;
    @Autowired
    private TemplateEngine templateEngine;

    /**
     * 通过模板发送
     * @param subject
     * @param setto
     * @param from
     * @param emailParam
     * @param template
     * @param imgPath
     * @throws MessagingException
     */
    @Async
    public void setEmail(String subject, String setto, String from, EmailParam emailParam,String template,String imgPath) throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper messageHelper=new MimeMessageHelper(mimeMessage,true);
        messageHelper.setSubject(subject);
        //发生附件
        //messageHelper.addAttachment(file.getName(),new File(file.getPath()));
        messageHelper.setTo(setto);//发送给谁
        messageHelper.setFrom(from);//发送者

        // 利用 Thymeleaf 模板构建 html 文本
        Context ctx = new Context();
        // 给模板的参数的上下文
        ctx.setVariable("emailParam", emailParam);
        String emailText = templateEngine.process(template, ctx);
        messageHelper.setText(emailText, true);
        //相对路径，项目的resources路径下
        ClassPathResource logoImage = new ClassPathResource(imgPath);
        messageHelper.addInline("logoImage", logoImage);
        mailSender.send(mimeMessage);
    }

    /**
     * 发送验证码
     * @param subject
     * @param text
     * @param setto
     * @param from
     * @throws MessagingException
     */
    @Async
    public void setEmailCode(String subject,String text,String setto,String from) throws MessagingException {

        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper messageHelper=new MimeMessageHelper(mimeMessage,true);
        messageHelper.setSubject(subject);
        messageHelper.setText(text,true);

        //发生附件
       // messageHelper.addAttachment(file.getName(),new File(file.getPath()));

        messageHelper.setTo(setto);//发送给谁
        System.out.println("setto="+setto);
        messageHelper.setFrom(from);//发送者
        mailSender.send(mimeMessage);
    }



}
