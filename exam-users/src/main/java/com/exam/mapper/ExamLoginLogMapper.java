package com.exam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.exam.pojo.ExamLoginLog;

/**
 * <p>
 * 用户日志表 Mapper 接口
 * </p>
 *
 * @author SIHAI0607
 * @since 2020-12-20
 */
public interface ExamLoginLogMapper extends BaseMapper<ExamLoginLog> {

}
