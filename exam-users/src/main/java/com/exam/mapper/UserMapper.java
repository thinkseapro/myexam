package com.exam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.exam.pojo.User;
import com.exam.vo.UserVo;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 普通用户表 Mapper 接口
 * </p>
 *
 * @author huwei
 * @since 2020-12-08
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

    UserVo getUserDetailed(String userid);

}
