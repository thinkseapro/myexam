package com.exam.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.exam.aspect.OperLog;
import com.exam.constant.ExamCode;
import com.exam.constant.Result;
import com.exam.pojo.ExamLoginLog;
import com.exam.pojo.User;
import com.exam.pojo.email.EmailParam;
import com.exam.service.IExamLoginLogService;
import com.exam.service.IUserService;
import com.exam.util.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.Session;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

/**
 * <p>
 * 普通用户表 前端控制器
 * </p>
 *
 * @author huwei
 * @since 2020-12-08
 */
@RestController
@RequestMapping("/user")
@CrossOrigin(maxAge = 3600)
public class UserController {

    @Autowired
    private IUserService UserService;
    @Autowired
    private IExamLoginLogService loginLogService;
    @Autowired
    private WebSocket webSocket;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private EmailUtils emailUtils;
    @Value("${spring.mail.username}")
    private String from;
    @Value("${templatePath}")
    private String templatePath;
    @Value("${imagePath}")
    private String imagePath;
    //默认密码
    @Value("${user.password}")
    private String default_password;

    @ApiOperation("登录")
    @RequestMapping("/login")
    @OperLog(operModul = "用户-登录",operType = "ADD",operDesc = "用户登录")
    public Result login(@RequestParam String username, @RequestParam String password){

        System.out.println(username+"----"+password);
        User user= UserService.getOne(new QueryWrapper<User>().eq("nickname",username).eq("is_student",0));
        System.out.println("user="+user);
        //用户名不存在
        if (CommonUtil.isEmpty(user)){
            return Result.result(ExamCode.Exam_Login_username_error);
        }
        //密码盐值加密
        String Salt_password= Md5Utils.md5Encryption(password,user.getSalt());
        //根据密码加盐值后进行查询
        User user2= UserService.getOne(new QueryWrapper<User>().eq("nickname",username).eq("password",Salt_password));
        //全局token
        String token=null;
        if (CommonUtil.isNotEmpty(user2)){
             //已重置密码状态
             if(user.getIsReset()==ExamCode.EXAM_CODE_ONE.getCode()){
                //生成token
                token=JwtUtil.createJWT(user.getUserId(),user.getNickname(),user.getUsername());
                //将token置入请求头
                System.out.println("token="+token);
                return Result.result(ExamCode.Exam_Login_PASSWORD_RESET,"token",token);
             }
            //查询该用户是否登录
            String redis_ip= (String) redisUtil.get(username);
            String ip=CommonUtil.getIpAddrByRequest(CommonUtil.getRequest());
            int count=loginLogService.count(new QueryWrapper<ExamLoginLog>().eq("ip",ip));
            if (count<1){
                try {
                    //模拟发送邮箱
                    String subject="异常登录提示"; //标题
                    String from="1505501720@qq.com";
                    EmailParam emailParam=new EmailParam();
                    emailParam.setEmail(user2.getEmail());
                    emailParam.setTitle("异常登录");
                    emailUtils.setEmail(subject,user2.getEmail(),from,emailParam,templatePath,imagePath);
                    System.out.println("异地登录 邮箱发送成功!");
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            System.out.println("ip="+ip);
            System.out.println("username="+username);
            if (redis_ip==null){ //有问题就用 redis_ip==null
                System.out.println("用户不存在...");
                //设置该用户存在300秒
                redisUtil.set(username,ip,300);
            }
            //这里--只能在服务器上使用
//            else if(ip.equals(redis_ip)){
//                System.out.println("用户重复登录...");
//                //设置该用户存在300秒
//                redisUtil.set(username,ip,300);
//            }
            else{
                //该用户存在，踢出之前登录用户
                //httpServletResponse.setHeader("Access-Control-Expose-Headers", "Exam-Authorization");
                try {
                    Session session = SocketManager.getInstance().getMapSession().get(username);
                    if (CommonUtil.isEmpty(session))  return Result.result(ExamCode.EXAM_LOGIN_EXIST);
                    session.getBasicRemote().sendText("return");
                    SocketManager.getInstance().getMapSession().remove(username);
                    webSocket.remove(username);
                    redisUtil.set(username,username,300);
                }catch (IOException e){
                    //踢人下线失败即用户重复登录
                    return Result.result(ExamCode.EXAM_LOGIN_EXIST);
                }
            }
            //创建用户登录日志
            loginLogService.add(user,CommonUtil.getRequest());
            //生成token
            token= JwtUtil.createJWT(user.getUserId(),user.getNickname(),user.getUsername());
            System.out.println("token= "+token);
        }else {
            System.out.println("密码错误!");
            return Result.result(ExamCode.Exam_Login_password_error);
        }
        return Result.result(ExamCode.Exam_Login_SUCCESS,"token",token);
    }

    @ApiOperation("退出登录")
    @RequestMapping("/exit")
    @OperLog(operModul = "用户-退出登录",operType = "Destroy",operDesc = "退出登录")
    public Result exit(@RequestParam String username){
        try {
            redisUtil.del(username);
            return Result.result(ExamCode.Exam_SUCCESS);
        }catch (Exception e){
            return Result.result(ExamCode.Exam_FAIL);
        }
    }

    @ApiOperation("修改用户密码")
    @RequestMapping("/updatePassword")
    @OperLog(operModul = "用户-修改密码",operType = "UPDATE",operDesc = "修改密码")
    public Result updatePassword(@RequestParam String password){
        String token=JwtUtil.GetToken();
        System.out.println("修改密码token="+token);
        //未带token,
        if (CommonUtil.isEmpty(token)){
            return Result.result(ExamCode.Exam_UPDATE_password_error);
        }
        String nickname=JwtUtil.getNickname(token);
        //密码盐值加密
        String salt=UUID.randomUUID().toString().replaceAll("-","");
        String Salt_password= Md5Utils.md5Encryption(password,salt);
        User user=new User();
        user.setSalt(salt);
        user.setPassword(Salt_password);
        user.setIsReset(0);
        boolean pds= UserService.update(user,new UpdateWrapper<User>().eq("nickname",nickname));
        if (!pds) throw new NullPointerException("密码更新失败");
        redisUtil.del(nickname);
        return Result.result(ExamCode.Exam_UPDATE_password_SUCCESS);
    }



    @ApiOperation("验证邮箱是否存在")
    @RequestMapping("/ismail")
    @OperLog(operModul = "用户-验证邮箱是否存在",operType = "OTHER",operDesc = "验证邮箱是否存在")
    public Result isEmail(@RequestParam String email){
        User user= UserService.getOne(new QueryWrapper<User>().eq("email",email));
        if (CommonUtil.isNotEmpty(user)) return Result.result(ExamCode.Exam_SUCCESS);
        return Result.result(ExamCode.Exam_FAIL);
    }


    @ApiOperation("生成邮箱重置token")
    @RequestMapping("/mail/resetPassword")
    @OperLog(operModul = "用户-根据邮箱重置密码生成token",operType = "OTHER",operDesc = "根据邮箱重置密码生成token")
    public Result resetPassword(@RequestParam String mail){
        String MailToken=JwtUtil.createMailJWT(mail);
        String encode = AESUtil.encryptIntoHexString(MailToken);
        System.out.println(MailToken);
        System.out.println("加密后token= "+encode);
        redisUtil.set(mail,MailToken,900);
        try {
            //用户每日限定邮箱验证次数--3次
            Integer num= (Integer) redisUtil.get(mail+"email");
            System.out.println("今日第"+num+"次");
            if (num==null) redisUtil.set(mail+"email",0,CommonUtil.getSecondsNextEarlyMorning());
            else if (3<=num) return Result.result(ExamCode.EXAM_EMAIL_MEET);
            else redisUtil.incr(mail+"email",1);
            //重置密码--发送邮箱
            String subject="重置账户登录密码"; //标题
            String from="1505501720@qq.com"; //发送方
            EmailParam emailParam=new EmailParam();
            emailParam.setEmail(mail);
            emailParam.setPassword(default_password);
            emailParam.setToken(encode);
            emailParam.setTitle("重置用户密码");
            emailUtils.setEmail(subject,mail,from,emailParam,templatePath,imagePath);
            System.out.println("邮箱发送成功!生成token: "+MailToken);
        }catch (Exception e){
            e.printStackTrace();
            return Result.result(ExamCode.Exam_SEND_ERROR);
        }
        return Result.result(ExamCode.Exam_SUCCESS);
    }


    @ApiOperation("验证token并根据邮箱重置密码")
    @RequestMapping("/mail/PasswordVerify/{tokens}")
    @OperLog(operModul = "用户-验证token并根据邮箱重置密码",operType = "UPDATE",operDesc = "验证token并根据邮箱重置密码")
    public Result mailVerify(@PathVariable String tokens){
        String token = AESUtil.decryptByHexString(tokens);
        System.out.println("收到token= "+token);
       boolean pd= JwtUtil.verify(token);
       if (!pd) Result.result(ExamCode.Exam_FAIL);
        String mail=JwtUtil.getMail(token);
        String redis_tokens= (String) redisUtil.get(mail);
        System.out.println("redis_tokens= "+redis_tokens);

        //redis不存在该token
       if (redis_tokens==null){
           return Result.result(ExamCode.Exam_FAIL);
       }
       else if(token.trim().equals(redis_tokens.trim())){
           User select_user= UserService.getOne(new QueryWrapper<User>().eq("email",mail));
           //判断该邮箱是否存在
           if (CommonUtil.isEmpty(select_user)) return Result.result(ExamCode.EXAM_EMAIL_NOT_EXIST);
           String password=Md5Utils.md5Hex3(default_password)+select_user.getNickname().length();
           //密码盐值加密
           String salt=UUID.randomUUID().toString().replaceAll("-","");
           String Salt_password= Md5Utils.md5Encryption(password,salt);
           User user=new User();
           user.setPassword(Salt_password);
           user.setSalt(salt);
           user.setIsReset(1);
           //更新密码为默认密码
           boolean pds= UserService.update(user,new UpdateWrapper<User>().eq("email",mail));
           if (!pds) throw new NullPointerException("密码更新失败");
           redisUtil.del(mail);
           return Result.result(ExamCode.Exam_SUCCESS);
       }
        //token发生变化
        return Result.result(ExamCode.Exam_SEND_ERROR);
    }

    @ApiOperation("验证手机号并发送验证码")
    @RequestMapping("/PhoneCode")
    @OperLog(operModul = "用户-验证手机号并发送验证码",operType = "OTHER",operDesc = "验证手机号并发送验证码")
    public Result PhoneCode(@RequestParam String phone){
        User user= UserService.getOne(new QueryWrapper<User>().eq("phone",phone));
        //手机号不存在
        if (CommonUtil.isEmpty(user)) return Result.result(ExamCode.EXAM_PHONE_ISNULL);
        else {
            String code = (String) redisUtil.get(phone);
            //该手机号验证码未过期
            if(!CommonUtil.isEmpty(code)){
               long time= redisUtil.getExpire(phone);
               return Result.result(ExamCode.EXAM_PHONE_EXIST,"time",time);
            }
            //生成验证码并存储到redis中
            code = UUID.randomUUID().toString().substring(0,6);
            HashMap<String,Object> param = new HashMap<>();
            param.put("code",code);
            System.out.println("code="+code);
            boolean isSend = SmsUtil.Send(phone,code);
            if (isSend){
                redisUtil.set(phone,code,120);
                return Result.result(ExamCode.EXAM_PHONE_SEND_SUCCESS);
            }
        }
        return Result.result(ExamCode.EXAM_PHONE_SEND_ERROR);
    }


    @ApiOperation("验证手机号和验证码，并根据手机号重置密码")
    @RequestMapping("/receiveCode")
    @OperLog(operModul = "用户-验证手机号和验证码，并根据手机号重置密码",operType = "OTHER",operDesc = "验证手机号和验证码，并根据手机号重置密码")
    public Result receiveCode(@RequestParam String phone,@RequestParam String code){
        String code_redis= (String) redisUtil.get(phone);
        if (CommonUtil.isNotEmpty(code_redis)){
            if (code.equals(code_redis)){
                User select_user= UserService.getOne(new QueryWrapper<User>().eq("phone",phone));
                //判断该手机号是否存在
                if (CommonUtil.isEmpty(select_user)) return Result.result(ExamCode.EXAM_PHONE_ISNULL);
                String password=Md5Utils.md5Hex3(default_password)+select_user.getNickname().length();
                //密码盐值加密
                String salt=UUID.randomUUID().toString().replaceAll("-","");
                String Salt_password= Md5Utils.md5Encryption(password,salt);
                User user=new User();
                user.setPassword(Salt_password);
                user.setSalt(salt);
                user.setIsReset(1);
                //更新密码为默认密码
                boolean pds= UserService.update(user,new UpdateWrapper<User>().eq("phone",phone));
                if (!pds) throw new NullPointerException("密码更新失败");
                return Result.result(ExamCode.EXAM_PHONE_CODE_SUCCESS,"message","您的密码已被初始化为"+default_password);
            }else {
                //验证码错误
                return Result.result(ExamCode.EXAM_PHONE_CODE_ERROR);
            }
        }else {
            //验证码超时或未发送验证码
            return Result.result(ExamCode.EXAM_PHONE_CODE_ERROR);
        }
    }

    @ApiOperation("根据Token获取登录名")
    @RequestMapping("/getNickname")
    @OperLog(operModul = "用户-根据Token获取登录名",operType = "OTHER",operDesc = "根据Token获取登录名")
    public Result getTokenNickname(){
        String token= JwtUtil.GetToken();
        System.out.println("yz/ :"+token);
        String username= JwtUtil.getNickname(token);
        return Result.result(ExamCode.Exam_SUCCESS,"name",username);
    }


    //验证token（可废弃）
    @CrossOrigin(maxAge = 3600)
    @RequestMapping("/ver/yz")
    public Result yz(HttpServletRequest request){
        String token=JwtUtil.GetToken();
        System.out.println("检验中="+token);
        System.out.println(Result.result(ExamCode.Exam_SUCCESS));
        return Result.result(ExamCode.Exam_SUCCESS);
    }

    @ApiOperation("查询用户信息")
    @RequestMapping("/ver/findUserByid")
    @OperLog(operModul = "用户-查询用户信息",operType = "SELECT",operDesc = "查询用户信息")
    public Result getUser(){
        String token= JwtUtil.GetToken();
        String userid= JwtUtil.getUserId(token);
        return UserService.getUserDetailed(userid);
    }


    @ApiOperation("用户头像修改")
    @RequestMapping(value = "/ver/uploadAvatar")
    @OperLog(operModul = "用户-用户头像修改",operType = "UPDATE",operDesc = "用户头像修改")
    public Result uploadAvatar(@RequestParam("file") String file){
        System.out.println(file);
        if (CommonUtil.isEmpty(file)){
            return Result.result(ExamCode.Exam_FAIL);
        }
        MultipartFile multipartFile=MultipartFileUtils.base64ToMultipart(file);
        System.out.println("file="+multipartFile.getName());
        String token= JwtUtil.GetToken();
        String userid= JwtUtil.getUserId(token);
        User user=UserService.getOne(new QueryWrapper<User>().eq("user_id",userid));
        if (CommonUtil.isEmpty(user)) Result.result(ExamCode.Exam_Login_username_error);
        redisUtil.del("getuser:"+user.getUserId());
        return UserService.uploadAvatar(multipartFile,userid);
    }

    @ApiOperation("发送邮箱验证码")
    @RequestMapping(value = "/ver/sentEmailCode")
    @OperLog(operModul = "用户-发送邮箱验证码",operType = "UPDATE",operDesc = "发送邮箱验证码")
    public Result sentEmailCode(@RequestParam String email){

        return UserService.sentEmailCode(email);
    }

    @ApiOperation("验证验证码并修改邮箱")
    @RequestMapping(value = "/ver/VerifyEmailCode")
    @OperLog(operModul = "用户-修改邮箱",operType = "UPDATE",operDesc = "修改邮箱")
    public Result VerifyEmailCode(@RequestParam String email,@RequestParam String newemail,@RequestParam String code){
        User user= UserService.getOne(new QueryWrapper<User>().eq("email",email));
        redisUtil.del("getuser:"+user.getUserId());
        return UserService.VerifyEmailCode(email,newemail,code);
    }

    @ApiOperation("发送手机验证码")
    @RequestMapping(value = "/ver/sentPhoneCode")
    @OperLog(operModul = "用户-发送手机验证码",operType = "UPDATE",operDesc = "发送手机验证码")
    public Result sentPhoneCode(@RequestParam String phone){

        return UserService.sentPhoneCode(phone);
    }

    @ApiOperation("验证验证码并修改手机号")
    @RequestMapping(value = "/ver/VerifyPhoneCode")
    @OperLog(operModul = "用户-修改手机",operType = "UPDATE",operDesc = "修改手机")
    public Result VerifyPhoneCode(@RequestParam String phone,@RequestParam String newphone,@RequestParam String code){
        User user= UserService.getOne(new QueryWrapper<User>().eq("phone",phone));
        redisUtil.del("getuser:"+user.getUserId());
        return UserService.VerifyPhoneCode(phone,newphone,code);
    }



}
