package com.exam.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.exam.pojo.ExamLoginLog;
import com.exam.pojo.User;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 用户日志表 服务类
 * </p>
 *
 * @author SIHAI0607
 * @since 2020-12-20
 */
public interface IExamLoginLogService extends IService<ExamLoginLog> {
    /**
     * 添加登入日志
     */
    void add(User user,HttpServletRequest request);

}
