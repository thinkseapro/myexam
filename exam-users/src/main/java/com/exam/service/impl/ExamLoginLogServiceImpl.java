package com.exam.service.impl;

import com.exam.pojo.User;
import com.exam.util.AddressUtil;
import eu.bitwalker.useragentutils.Browser;
import eu.bitwalker.useragentutils.UserAgent;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.exam.mapper.ExamLoginLogMapper;
import com.exam.pojo.ExamLoginLog;
import com.exam.service.IExamLoginLogService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.exam.util.CommonUtil;
import eu.bitwalker.useragentutils.OperatingSystem;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * <p>
 * 用户日志表 服务实现类
 * </p>
 *
 * @author SIHAI0607
 * @since 2020-12-20
 */
@Service
public class ExamLoginLogServiceImpl extends ServiceImpl<ExamLoginLogMapper, ExamLoginLog> implements IExamLoginLogService {

    @Autowired
    private ExamLoginLogMapper loginLogMapper;


    @Override
    public void add(User user,HttpServletRequest request) {
        loginLogMapper.insert(createLoginLog(user,request));
    }

    /**
     * 创建登入日志
     * @param
     * @return
     */
    public static ExamLoginLog createLoginLog(User user, HttpServletRequest request) {
        ExamLoginLog loginLog = new ExamLoginLog();
        loginLog.setUserId(user.getUserId());
        loginLog.setUsername(user.getUsername());
        loginLog.setNickname(user.getNickname());
        loginLog.setCreateTime(new Date());
        loginLog.setIp(CommonUtil.getIpAddrByRequest(request));
        loginLog.setLocation(AddressUtil.getCityInfo(CommonUtil.getIpAddrByRequest(request)));
        // 获取客户端操作系统
        UserAgent userAgent = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
        Browser browser = userAgent.getBrowser();
        OperatingSystem os = userAgent.getOperatingSystem();
        loginLog.setUserSystem(os.getName());
        loginLog.setUserBrowser(browser.getName());

        System.out.println("登入日志信息: " + loginLog);
        return loginLog;
    }
}
