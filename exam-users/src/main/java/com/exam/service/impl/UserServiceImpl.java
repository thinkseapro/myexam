package com.exam.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.exam.constant.ExamCode;
import com.exam.constant.Result;
import com.exam.mapper.UserMapper;
import com.exam.pojo.User;
import com.exam.pojo.email.EmailParam;
import com.exam.service.IUserService;
import com.exam.util.*;
import com.exam.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 普通用户表 服务实现类
 * </p>
 *
 * @author huwei
 * @since 2020-12-08
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private OssUtil ossUtil;
    @Autowired
    private EmailUtils emailUtils;
    @Autowired
    private SnowflakeUtil snowflakeUtil;
    @Autowired
    private RedisUtil redisUtil;


    @Override
    public Result uploadAvatar(MultipartFile file, String userId) {
        if (CommonUtil.isEmpty(file)) {
            return Result.result(ExamCode.Exam_ISNULL);
        }
        String url = ossUtil.uploadImg2Oss(file);
        //updateUrl(userId, imgUrls[0]);
        User user=new User();
        user.setUserId(userId);
        user.setUserImage(url);
        int num= userMapper.updateById(user);
        if (num==1) return Result.result(ExamCode.Exam_SUCCESS);
        return Result.result(ExamCode.Exam_FAIL);
    }

    @Override
    public Result getUserDetailed(String userid) {
        UserVo userVo=null;
        try {
            userVo= (UserVo) redisUtil.get("getuser:"+userid);
        }catch (Exception e){
            userVo=null;
        }
        if (CommonUtil.isEmpty(userVo)){
            userVo=userMapper.getUserDetailed(userid);
            if (CommonUtil.isEmpty(userVo)) return Result.result(ExamCode.Exam_ISNULL);
            redisUtil.set("getuser:"+userid,userVo,360);
        }
        return Result.result(ExamCode.Exam_SUCCESS,"user",userVo);
    }

    @Override
    public Result sentEmailCode(String email) {
        String subject="小鹿考试系统--更换邮箱"; //标题
        String from="1505501720@qq.com"; //发送方
        String code=snowflakeUtil.uuid().substring(0,6);
        String text="正在更换邮箱，您的验证码为:"+code;
        try {
            emailUtils.setEmailCode(subject,text,email,from);
            redisUtil.set(email+"em",code,180);
        }catch (Exception e){
            return Result.result(ExamCode.Exam_SEND_ERROR);
        }
        return Result.result(ExamCode.Exam_SUCCESS);
    }

    @Override
    public Result VerifyEmailCode(String email, String newemail, String code) {
        String codes= (String) redisUtil.get(email+"em");
        if (CommonUtil.isEmpty(codes)) return Result.result(ExamCode.EXAM_Expired);
        //验证码错误
        else if (!code.equals(codes)){
            return Result.result(ExamCode.EXAM_EMAIL_CODE_ERROR);
        }
        User user=new User();
        user.setEmail(newemail);
        int num=userMapper.update(user,new QueryWrapper<User>().eq("email",email));
        //修改失败
        if (num!=1) return Result.result(ExamCode.Exam_FAIL);
        return Result.result(ExamCode.Exam_SUCCESS);
    }

    @Override
    public Result sentPhoneCode(String phone) {
        String code=snowflakeUtil.uuid().substring(0,6);
        boolean pd=false;
        try {
            redisUtil.set(phone+"ph",code,180);
            pd= SmsUtil.Send(phone,code);
            if (!pd) return Result.result(ExamCode.Exam_SEND_ERROR);
        }catch (Exception e){
            redisUtil.del(phone+"ph");
            return Result.result(ExamCode.Exam_SEND_ERROR);
        }

        return Result.result(ExamCode.Exam_SUCCESS);
    }

    @Override
    public Result VerifyPhoneCode(String phone, String newphone, String code) {
        String codes= (String) redisUtil.get(phone+"ph");
        if (CommonUtil.isEmpty(codes)) return Result.result(ExamCode.EXAM_Expired);
            //验证码错误
        else if (!code.equals(codes)){
            return Result.result(ExamCode.EXAM_EMAIL_CODE_ERROR);
        }
        User user=new User();
        user.setPhone(newphone);
        int num=userMapper.update(user,new QueryWrapper<User>().eq("phone",phone));
        //修改失败
        if (num!=1) return Result.result(ExamCode.Exam_FAIL);
        return Result.result(ExamCode.Exam_SUCCESS);
    }
}
