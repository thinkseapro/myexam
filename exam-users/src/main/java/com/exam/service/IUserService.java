package com.exam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.exam.constant.Result;
import com.exam.pojo.User;
import com.exam.vo.UserVo;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 普通用户表 服务类
 * </p>
 *
 * @author huwei
 * @since 2020-12-08
 */
public interface IUserService extends IService<User> {

   public Result uploadAvatar(MultipartFile file, String userId);

   //查询用户详情
   Result getUserDetailed(String userid);

   //发送邮箱验证码
   Result sentEmailCode(String email);

   //修改邮箱
   Result VerifyEmailCode(String email,String newphone,String code);

   //发送手机验证码
   Result sentPhoneCode(String phone);

   //修改手机
   Result VerifyPhoneCode(String phone,String newphone,String code);
}
