package com.exam.filter;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.exam.util.AESUtil;
import com.exam.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

/**
 * @description: 公共的权限过滤filter
 * @Author: huwei
 */
@Component
public class ApiGlobalFilter implements GlobalFilter, Ordered {

    @Autowired
    private JwtUtil JwtUtil;

    /**
     * 不进行token校验的地址
     */
    @Value("#{'${jwt.ignoreUrlList}'.split(',')}")
    public List<String> ignoreUrl;


    /**
     * 拦截所有的请求头
     * @param exchange
     * @param chain
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        String url = exchange.getRequest().getPath().toString();
        System.out.println("拦截所有的请求头="+url);
        if(url.endsWith(".css")||url.endsWith(".js")||url.endsWith(".jpg")||url.endsWith(".svg")
           ||url.endsWith(".gif")||url.endsWith(".png")||url.endsWith(".html") || url.endsWith(".woff") || url.endsWith(".woff2")
           || url.endsWith(".tff")
           || url.startsWith("/user/ver/no") || url.startsWith("/Authority")){
            System.out.println("放行静态资源");
            return chain.filter(exchange);
        }
        boolean status = CollectionUtil.contains(ignoreUrl, url);
        if (!status){
            String token = exchange.getRequest().getHeaders().getFirst("Exam-Authorization");
            if (StrUtil.isBlank(token)){
                Map<String, List<String>> map=new HashMap<>();
                map.putAll(exchange.getRequest().getQueryParams());
                try {
                    token=map.get("parameter").toString().replace("[","").replace("]","").trim();
                }catch (Exception e){
                    token=null;
                }
            }
            System.out.println("token="+token);
            //token解密
            if (!StrUtil.isBlank(token)){
                try {
                    token=AESUtil.decryptByHexString(token);
                }catch (Exception e){
                    token=null;
                }
            }
            ServerHttpResponse response = exchange.getResponse();
            //没有数据
            if (StrUtil.isBlank(token)) {
                JSONObject message = new JSONObject();
                message.put("code", "403");
                message.put("message", "无token");
                System.out.println("无token");
                byte[] bits = message.toString().getBytes(StandardCharsets.UTF_8);
                DataBuffer buffer = response.bufferFactory().wrap(bits);
                response.setStatusCode(HttpStatus.UNAUTHORIZED);
                response.getHeaders().add("Content-Type", "text/json;charset=UTF-8");
                String urls = "/clientfeign/Authority/421";
                //303状态码表示由于请求对应的资源存在着另一个URI，应使用GET方法定向获取请求的资源
                response.setStatusCode(HttpStatus.SEE_OTHER);
                response.getHeaders().set(HttpHeaders.LOCATION, urls);
                return response.setComplete();
               // return response.writeWith(Mono.just(buffer));
                //有数据
            }else {
                //校验token
                Map<String, String> tokenInfoMap = JwtUtil.verifyJWT(token);
                if (JwtUtil.TOKEN_ERROR.equals(tokenInfoMap.get("code"))){
                    JSONObject message = new JSONObject();
                    message.put("code", "403");
                    message.put("message", "token错误");
                    byte[] bits = message.toString().getBytes(StandardCharsets.UTF_8);
                    DataBuffer buffer = response.bufferFactory().wrap(bits);
                    response.setStatusCode(HttpStatus.UNAUTHORIZED);
                    response.getHeaders().add("Content-Type", "text/json;charset=UTF-8");
                    String urls = "/clientfeign/Authority/422";
                    //303状态码表示由于请求对应的资源存在着另一个URI，应使用GET方法定向获取请求的资源
                    response.setStatusCode(HttpStatus.SEE_OTHER);
                    response.getHeaders().set(HttpHeaders.LOCATION, urls);
                    return response.setComplete();
                   // return response.writeWith(Mono.just(buffer));
                }else if (JwtUtil.TOKEN_EXPIRED.equals(tokenInfoMap.get("code"))){
                    JSONObject message = new JSONObject();
                    message.put("code", "403");
                    message.put("message", "token过期");
                    System.out.println("token过期");
                    byte[] bits = message.toString().getBytes(StandardCharsets.UTF_8);
                    DataBuffer buffer = response.bufferFactory().wrap(bits);
                    response.setStatusCode(HttpStatus.UNAUTHORIZED);
                    response.getHeaders().add("Content-Type", "text/json;charset=UTF-8");
                    String urls = "/clientfeign/Authority/423";
                    //303状态码表示由于请求对应的资源存在着另一个URI，应使用GET方法定向获取请求的资源
                    response.setStatusCode(HttpStatus.SEE_OTHER);
                    response.getHeaders().set(HttpHeaders.LOCATION, urls);
                    return response.setComplete();
                    //return response.writeWith(Mono.just(buffer));
                }
                //将数据返回给下级服务器
                String finalToken = token.trim();
                Consumer<HttpHeaders> httpHeaders = httpHeader -> {
                    System.out.println("传给下游token="+finalToken);
                    httpHeader.set("Exam-Authorization", finalToken);
                };
                //将现在的request，添加当前身份
                ServerHttpRequest mutableReq = exchange.getRequest().mutate().headers(httpHeaders).build();
                ServerWebExchange mutableExchange = exchange.mutate().request(mutableReq).build();
                System.out.println("检验成功");
                return chain.filter(mutableExchange);
            }
        }
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 10101;
    }
}
