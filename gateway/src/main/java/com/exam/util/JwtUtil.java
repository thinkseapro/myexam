package com.exam.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * GateWay---JWT工具类
 **/
@Component
public class JwtUtil {

    private static final String key="BDHHJBHCB73AH0YB"; //密钥加盐

    private static Map<String, String> map = new HashMap<String, String>();

    /**
     * token校验通过
     */
    public final String TOKEN_CORRECT = "TOKEN_CORRECT";
    /**
     * token错误
     */
    public final String TOKEN_ERROR = "TOKEN_ERROR";
    /**
     * token过期
     */
    public final String TOKEN_EXPIRED = "TOKEN_EXPIRED";

    /**
     * 校验token
     * @param token
     * @return
     */
    public  Map<String, String> verifyJWT(String token){
        Algorithm algorithm = Algorithm.HMAC256(key);
        JWTVerifier verifier = JWT.require(algorithm).build();
        try {
            DecodedJWT jwt = verifier.verify(token);
            map.put("code", TOKEN_CORRECT);
        }catch (TokenExpiredException e){
            map.put("code", TOKEN_EXPIRED);
        }catch (Exception e){
            map.put("code", TOKEN_ERROR);
        }finally {
            return map;
        }
    }




}