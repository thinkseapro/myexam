/*
SQLyog Ultimate v11.24 (32 bit)
MySQL - 5.6.37 : Database - myexam
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`myexam` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `myexam`;

/*Table structure for table `exam_class` */

DROP TABLE IF EXISTS `exam_class`;

CREATE TABLE `exam_class` (
  `class_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '班级id',
  `class_name` varchar(50) DEFAULT NULL COMMENT '班级名称',
  `user_id` varchar(50) DEFAULT NULL COMMENT '班主任用户ID',
  `grade_id` int(11) DEFAULT NULL COMMENT '年级id',
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='班级表';

/*Data for the table `exam_class` */

insert  into `exam_class`(`class_id`,`class_name`,`user_id`,`grade_id`) values (1,'lg19','uuid2',1),(2,'lg20','uuid2',2),(3,'lg30','uuid2',3);

/*Table structure for table `exam_enterprise_details` */

DROP TABLE IF EXISTS `exam_enterprise_details`;

CREATE TABLE `exam_enterprise_details` (
  `enterprise_details_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `examination_id` int(11) DEFAULT NULL COMMENT '试卷主键',
  `enterprise_details_price` float DEFAULT NULL COMMENT '价格',
  ` enterprise_details_buy_num` int(11) DEFAULT NULL COMMENT '已售出数量',
  `enterprise_details_limit_num` int(11) DEFAULT NULL COMMENT '限量数',
  `enterprise_details_type` int(11) DEFAULT NULL COMMENT '0:限量优惠试题，1:经典试题',
  PRIMARY KEY (`enterprise_details_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='企业试卷详情表';

/*Data for the table `exam_enterprise_details` */

insert  into `exam_enterprise_details`(`enterprise_details_id`,`examination_id`,`enterprise_details_price`,` enterprise_details_buy_num`,`enterprise_details_limit_num`,`enterprise_details_type`) values (1,1,2,NULL,3,4);

/*Table structure for table `exam_enterprise_orders` */

DROP TABLE IF EXISTS `exam_enterprise_orders`;

CREATE TABLE `exam_enterprise_orders` (
  `enterprise_orders_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `enterprise_orders_number` varchar(50) DEFAULT NULL COMMENT '订单编号',
  `enterprise_details_id` int(11) DEFAULT NULL COMMENT '企业试卷详情主键id',
  `user_id` varchar(50) DEFAULT NULL COMMENT '用户id',
  `enterprise_orders_price` float DEFAULT NULL COMMENT '订单价格',
  `ispay` int(11) DEFAULT NULL COMMENT '是否付款 0:未付款 1:已付款',
  `enterprise_orders_createtime` datetime DEFAULT NULL COMMENT '订单创建时间',
  PRIMARY KEY (`enterprise_orders_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='企业试卷订单表';

/*Data for the table `exam_enterprise_orders` */

/*Table structure for table `exam_error` */

DROP TABLE IF EXISTS `exam_error`;

CREATE TABLE `exam_error` (
  `error_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `user_id` varchar(50) DEFAULT NULL COMMENT '用户主键',
  `topic_id` int(11) DEFAULT NULL COMMENT '题目id',
  `correct_answer` int(11) DEFAULT NULL COMMENT '用户答案A,..,C',
  `error_sum` int(11) unsigned zerofill DEFAULT '00000000000' COMMENT '错误次数--默认0',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `pattern` int(11) DEFAULT NULL COMMENT '(模式分类0:考试,1:练习)',
  PRIMARY KEY (`error_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='错题表';

/*Data for the table `exam_error` */

insert  into `exam_error`(`error_id`,`user_id`,`topic_id`,`correct_answer`,`error_sum`,`create_time`,`pattern`) values (1,'1',1,1,00000000023,'2020-12-04 17:37:47',0),(2,NULL,NULL,NULL,00000000000,NULL,NULL);

/*Table structure for table `exam_examination` */

DROP TABLE IF EXISTS `exam_examination`;

CREATE TABLE `exam_examination` (
  `examination_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '试卷主键',
  `exam_name` varchar(50) DEFAULT NULL COMMENT '试卷名称',
  `user_id` varchar(50) DEFAULT NULL COMMENT '出卷人id',
  `subject_id` varchar(50) DEFAULT NULL COMMENT '科目id',
  `class_id` int(11) DEFAULT NULL COMMENT '班级id',
  `duration` varchar(50) DEFAULT NULL COMMENT '时长',
  `examination_total_score` varchar(10) DEFAULT NULL COMMENT '总分',
  `topic_sum` int(11) DEFAULT NULL COMMENT '题数',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `common` varchar(200) DEFAULT NULL COMMENT '描述',
  `examination_flag` int(11) DEFAULT NULL COMMENT '0:本校试卷,1:企业试卷',
  PRIMARY KEY (`examination_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='试卷表';

/*Data for the table `exam_examination` */

insert  into `exam_examination`(`examination_id`,`exam_name`,`user_id`,`subject_id`,`class_id`,`duration`,`examination_total_score`,`topic_sum`,`start_time`,`end_time`,`create_time`,`common`,`examination_flag`) values (1,'期末考试','1','1',1,'1','100',3,'2020-12-02 17:35:58','2020-12-31 17:36:04','2020-12-04 17:36:07','无',0),(2,'打工人初级测试','1','1',1,'1','100',3,'2020-12-07 11:49:24','2020-12-07 12:49:34','2020-12-01 11:50:00','无',0);

/*Table structure for table `exam_exercise_details` */

DROP TABLE IF EXISTS `exam_exercise_details`;

CREATE TABLE `exam_exercise_details` (
  `exam_exercise_details_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '考试|练习 详情id',
  `user_id` varchar(50) DEFAULT NULL COMMENT '用户id',
  `duration` datetime DEFAULT NULL COMMENT '用时时长',
  `details_state` int(11) DEFAULT NULL COMMENT '状态--0:练习中 1:已提交',
  `details_score` varchar(10) DEFAULT NULL COMMENT '分数',
  `end_time` datetime DEFAULT NULL COMMENT '提交时间',
  `subject_id` int(11) DEFAULT NULL COMMENT '科目id',
  `flag` int(11) DEFAULT NULL COMMENT '0:练习详情,1考试详情',
  `examination_id` int(11) DEFAULT NULL COMMENT '<练习:为空,考试:试卷主键>',
  `is_exception` int(11) DEFAULT NULL COMMENT '作弊(是否异常<练习:为空,考试:0异常||1正常>)',
  PRIMARY KEY (`exam_exercise_details_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='考试|练习 详情表';

/*Data for the table `exam_exercise_details` */

insert  into `exam_exercise_details`(`exam_exercise_details_id`,`user_id`,`duration`,`details_state`,`details_score`,`end_time`,`subject_id`,`flag`,`examination_id`,`is_exception`) values (1,'1','2020-12-03 17:48:04',1,NULL,'2020-12-03 17:48:42',1,1,NULL,1),(2,'1','2020-12-07 11:50:29',1,NULL,'2020-12-07 11:50:44',1,1,NULL,1);

/*Table structure for table `exam_exercise_topic` */

DROP TABLE IF EXISTS `exam_exercise_topic`;

CREATE TABLE `exam_exercise_topic` (
  `exam_exercise_topic_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '(考试|练习 题目id)',
  `user_id` varchar(50) DEFAULT NULL COMMENT '用户id',
  `topic_id` int(11) DEFAULT NULL COMMENT '题目id',
  `correct_answer` varchar(50) DEFAULT NULL COMMENT '正确答案',
  `exercise_answer` varchar(50) DEFAULT NULL COMMENT '作答答案--A0、B1、C2',
  `exam_exercise_details_id` int(11) DEFAULT NULL COMMENT '(考试|练习 详情id)',
  PRIMARY KEY (`exam_exercise_topic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `exam_exercise_topic` */

/*Table structure for table `exam_grade` */

DROP TABLE IF EXISTS `exam_grade`;

CREATE TABLE `exam_grade` (
  `grade_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '年级id',
  `grade_name` varchar(50) DEFAULT NULL COMMENT '年级名称',
  PRIMARY KEY (`grade_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='年级表';

/*Data for the table `exam_grade` */

insert  into `exam_grade`(`grade_id`,`grade_name`) values (1,'一年级'),(2,'二年级'),(3,'三年级');

/*Table structure for table `exam_level` */

DROP TABLE IF EXISTS `exam_level`;

CREATE TABLE `exam_level` (
  `level_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `level_name` varchar(50) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='难易度表';

/*Data for the table `exam_level` */

insert  into `exam_level`(`level_id`,`level_name`) values (1,'新手'),(2,'老手'),(3,'地狱'),(4,'死亡');

/*Table structure for table `exam_menu` */

DROP TABLE IF EXISTS `exam_menu`;

CREATE TABLE `exam_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单id',
  `parent_id` int(11) DEFAULT NULL COMMENT '上级菜单salt,0没有上级菜单id',
  `menu_name` varchar(50) DEFAULT NULL COMMENT '菜单/按钮名称',
  `url` varchar(50) DEFAULT NULL COMMENT '菜单地址',
  `perms` varchar(50) DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(50) DEFAULT 'el-icon-s-flag' COMMENT '图标(默认旗帜)',
  `type` int(11) DEFAULT '0' COMMENT '类型 0菜单 1按钮(默认是菜单)',
  `order_num` int(11) DEFAULT '1' COMMENT '排序',
  `disabled` int(11) DEFAULT '0' COMMENT '0未禁,1禁用',
  `open` int(11) DEFAULT '0' COMMENT '菜单栏,0不展开 1展开',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='系统菜单表(后台侧边栏)';

/*Data for the table `exam_menu` */

insert  into `exam_menu`(`menu_id`,`parent_id`,`menu_name`,`url`,`perms`,`icon`,`type`,`order_num`,`disabled`,`open`,`create_time`,`modify_time`) values (1,0,'首页1','/homePage','homePage:view','el-icon-s-home',0,1,0,0,'2020-12-07 17:23:49','2020-12-07 17:23:49'),(2,0,'系统管理',NULL,NULL,'el-icon-setting',0,2,0,0,'2020-12-07 17:35:47','2020-12-07 17:35:47'),(3,0,'业务管理',NULL,NULL,'el-icon-s-goods',0,3,0,0,'2020-12-07 17:35:48','2020-12-07 17:35:48'),(4,0,'日志管理',NULL,NULL,'el-icon-camera',0,4,0,0,'2020-12-07 17:36:11','2020-12-07 17:36:11'),(5,0,'其他管理',NULL,NULL,'el-icon-s-marketing',0,5,0,0,'2020-12-07 17:36:22','2020-12-07 17:36:22'),(6,2,'用户管理','/users','users:view','el-icon-user',0,1,0,0,'2020-12-07 18:20:30','2020-12-07 18:20:30'),(7,2,'角色管理','/roles','roles:view','el-icon-postcard',0,2,0,0,'2020-12-07 18:20:37','2020-12-07 18:20:37'),(8,2,'班级管理','/class','class:view','el-icon-s-home',0,3,0,0,'2020-12-07 18:21:10','2020-12-07 18:21:10'),(9,6,'用户添加',NULL,'user:add','el-icon-plus',1,1,0,0,'2020-12-07 18:25:00','2020-12-07 18:25:00'),(10,6,'用户删除',NULL,'user:delete','el-icon-picture',1,1,0,0,'2020-12-07 18:25:17','2020-12-07 18:25:17'),(11,6,'用户编辑',NULL,'user:edit','el-icon-video-camera-solid',1,1,0,0,'2020-12-07 18:25:42','2020-12-07 18:25:42'),(12,6,'用户导出',NULL,'user:export','el-icon-edit',1,1,0,0,'2020-12-07 18:27:35','2020-12-07 18:27:35'),(13,6,'分配角色',NULL,'user:assign','el-icon-s-promotion',1,1,0,0,'2020-12-07 18:29:29','2020-12-07 18:29:29'),(14,3,'题目管理','/topic','topic:view','el-icon-date',0,1,0,0,'2020-12-07 18:29:29','2020-12-07 18:29:29'),(15,3,'试卷管理','/examination','examination:view','el-icon-s-claim',0,2,0,0,'2020-12-07 18:31:33','2020-12-07 18:31:33'),(16,4,'登入日志','/loginLog','loginLog:view','el-icon-date',0,1,0,0,'2020-12-07 18:37:47','2020-12-07 18:37:47'),(17,4,'操作日志','/logs','logs:view','el-icon-edit',0,2,0,0,'2020-12-07 18:38:07','2020-12-07 18:38:07'),(18,5,'图标管理','/icons','icons:view','el-icon-more-outline',0,1,0,0,'2020-12-07 18:39:24','2020-12-07 18:39:24'),(19,5,'swagger文档	','/swagger','swagger:view','el-icon-star-on',0,2,0,0,'2020-12-07 18:39:40','2020-12-07 18:39:40'),(20,0,'一级菜单',NULL,NULL,'el-icon-s-flag',0,6,0,0,'2020-12-08 09:57:06','2020-12-08 09:57:06'),(21,20,'二级菜单',NULL,NULL,'el-icon-s-flag',0,1,0,0,'2020-12-08 09:57:36','2020-12-08 09:57:36'),(22,21,'三级级菜单','/testMenu','testMenu:view','el-icon-s-flag',0,1,0,0,'2020-12-08 09:58:07','2020-12-08 09:58:07');

/*Table structure for table `exam_option_data` */

DROP TABLE IF EXISTS `exam_option_data`;

CREATE TABLE `exam_option_data` (
  `option_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `option_flag` int(11) DEFAULT NULL COMMENT '标识A-0、B-1、C-2',
  `option_name` varchar(200) DEFAULT NULL COMMENT '选项内容',
  `topic_id` int(11) DEFAULT NULL COMMENT '题目id',
  `is_answer` int(11) DEFAULT NULL COMMENT '该选项是否为正确答案--0:错误 1:正确',
  PRIMARY KEY (`option_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='选项内容表';

/*Data for the table `exam_option_data` */

insert  into `exam_option_data`(`option_id`,`option_flag`,`option_name`,`topic_id`,`is_answer`) values (1,0,'1号',1,1),(2,1,'2号',1,0),(3,2,'3号',1,0),(4,3,'四号',1,0),(5,0,'好的',2,1);

/*Table structure for table `exam_role` */

DROP TABLE IF EXISTS `exam_role`;

CREATE TABLE `exam_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色主键',
  `role_name` varchar(50) DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(300) DEFAULT NULL COMMENT '角色描述',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `status` int(1) DEFAULT NULL COMMENT '是否可用,0不可用,1可用',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `exam_role` */

insert  into `exam_role`(`role_id`,`role_name`,`remark`,`create_time`,`modify_time`,`status`) values (1,'超级管理员','拥有系统各个模块的最高权限(crud)','2020-12-07 10:28:30','2020-12-07 10:28:32',1),(2,'老师','只\'拥有本班crud\'和\'其他班级只读\'的权限(若是这样有点特殊)','2020-12-07 10:28:08','2020-12-07 10:28:08',1),(3,'学生','前台用户人员','2020-12-07 10:28:27','2020-12-07 10:28:27',1);

/*Table structure for table `exam_role_menu` */

DROP TABLE IF EXISTS `exam_role_menu`;

CREATE TABLE `exam_role_menu` (
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `menu_id` int(11) NOT NULL COMMENT '菜单/按钮ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `exam_role_menu` */

insert  into `exam_role_menu`(`role_id`,`menu_id`) values (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),(1,10),(1,11),(1,12),(1,13),(1,14),(1,15),(1,18),(1,19),(1,20),(1,21),(1,22),(1,17),(1,16);

/*Table structure for table `exam_subject` */

DROP TABLE IF EXISTS `exam_subject`;

CREATE TABLE `exam_subject` (
  `subject_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `subject_name` varchar(50) DEFAULT NULL COMMENT '科目名称',
  PRIMARY KEY (`subject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='科目表(语文,数学)：--科目为0为混合考试';

/*Data for the table `exam_subject` */

insert  into `exam_subject`(`subject_id`,`subject_name`) values (1,'语文'),(2,'数学'),(3,'历史'),(4,'地理');

/*Table structure for table `exam_topic` */

DROP TABLE IF EXISTS `exam_topic`;

CREATE TABLE `exam_topic` (
  `topic_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '题目主键',
  `user_id` varchar(50) DEFAULT NULL COMMENT '创建者id',
  `topic_title` varchar(50) DEFAULT NULL COMMENT '题目名称',
  `level_id` int(11) DEFAULT NULL COMMENT '难易度id',
  `type_id` int(11) DEFAULT NULL COMMENT '类型id',
  `subject_id` int(11) DEFAULT NULL COMMENT '科目id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `topic_flag` int(11) DEFAULT NULL COMMENT '0:练习题目 1:考试题目 2: 企业题目',
  `topic_score` varchar(10) DEFAULT NULL COMMENT '该题分数',
  `parse` varchar(200) DEFAULT NULL COMMENT '答案解析',
  `common` varchar(200) DEFAULT NULL COMMENT '描述',
  `is_delete` int(11) DEFAULT NULL COMMENT '是否删除delete',
  `grade_id` int(11) DEFAULT NULL COMMENT '题目 所属年级',
  PRIMARY KEY (`topic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='题目表';

/*Data for the table `exam_topic` */

insert  into `exam_topic`(`topic_id`,`user_id`,`topic_title`,`level_id`,`type_id`,`subject_id`,`create_time`,`topic_flag`,`topic_score`,`parse`,`common`,`is_delete`,`grade_id`) values (1,'1','今天有课没？',1,1,1,'2020-12-01 17:13:05',1,'5','无','无',0,1),(2,'1','今天是几号',1,1,1,'2020-12-02 17:17:12',1,'5','无','无',0,1),(3,'1','黎曼猜想怎么才能解开',3,1,3,'2020-12-01 17:18:42',2,'5','无','无',0,1);

/*Table structure for table `exam_topic_examination` */

DROP TABLE IF EXISTS `exam_topic_examination`;

CREATE TABLE `exam_topic_examination` (
  `te_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `topic_id` int(11) DEFAULT NULL COMMENT '题目主键',
  `examination_id` varchar(50) DEFAULT NULL COMMENT '试卷主键',
  PRIMARY KEY (`te_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='题目+试卷中间表';

/*Data for the table `exam_topic_examination` */

insert  into `exam_topic_examination`(`te_id`,`topic_id`,`examination_id`) values (1,1,'1'),(2,2,'1'),(3,3,'1');

/*Table structure for table `exam_topic_tick` */

DROP TABLE IF EXISTS `exam_topic_tick`;

CREATE TABLE `exam_topic_tick` (
  `topic_tick_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '错题反馈id',
  `topic_id` int(11) DEFAULT NULL COMMENT '题目id',
  `user_id` varchar(50) DEFAULT NULL COMMENT '用户主键',
  `topic_name` varchar(200) DEFAULT NULL COMMENT '题目名称',
  `topic_tick_common` varchar(200) DEFAULT NULL COMMENT '反馈信息',
  `topic_tick_state` int(11) DEFAULT NULL COMMENT '修改的状态--0:未修改 1:已修改',
  PRIMARY KEY (`topic_tick_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='错题反馈表';

/*Data for the table `exam_topic_tick` */

/*Table structure for table `exam_type` */

DROP TABLE IF EXISTS `exam_type`;

CREATE TABLE `exam_type` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type_name` varchar(50) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='题目类型表(单选，多选)';

/*Data for the table `exam_type` */

insert  into `exam_type`(`type_id`,`type_name`) values (1,'单选'),(2,'多选');

/*Table structure for table `exam_user` */

DROP TABLE IF EXISTS `exam_user`;

CREATE TABLE `exam_user` (
  `user_id` varchar(50) NOT NULL COMMENT '用户主键',
  `username` varchar(50) DEFAULT NULL COMMENT '真实姓名',
  `nickname` varchar(50) DEFAULT NULL COMMENT '昵称(用于登录,唯一)',
  `sex` int(11) DEFAULT NULL COMMENT '性别,0男,1女',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `user_image` varchar(50) DEFAULT NULL COMMENT '用户头像地址',
  `phone` varchar(50) DEFAULT NULL COMMENT '手机号',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `birth` date DEFAULT NULL COMMENT '生日',
  `class_id` int(11) DEFAULT NULL COMMENT '''学生''所属班级id(0代表该用户不是学生)',
  `is_audit` int(11) DEFAULT '0' COMMENT '是否审核通过0未审核,1已审核',
  `audit_user_id` varchar(50) DEFAULT 'uuid1' COMMENT '审核者',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_lock` int(11) DEFAULT '0' COMMENT '状态,0未锁定, 1禁用锁定(注意,只有审核通过和该账户没被锁定才能登陆)-->后台有个是否禁用的按钮',
  `id_delete` int(11) DEFAULT '0' COMMENT '是否删除,0未删除,1已删除',
  `is_user_manager` int(11) DEFAULT '0' COMMENT '(只是作为一个标识而已)是否能够进入后台管理界面(0学生不能进入,1可以进入)-->所对应的角色在(user_role中对应)',
  `last_login` date DEFAULT NULL COMMENT '最后一次登录时间',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='普通用户表';

/*Data for the table `exam_user` */

insert  into `exam_user`(`user_id`,`username`,`nickname`,`sex`,`password`,`user_image`,`phone`,`email`,`birth`,`class_id`,`is_audit`,`audit_user_id`,`create_time`,`modify_time`,`is_lock`,`id_delete`,`is_user_manager`,`last_login`) values ('uuid1','admin','admin',1,'f0170504c4c23676431e940e14282f3c','image1','15723456547','1@qq.com','2020-12-07',0,1,'uuid1','2020-12-07 16:54:22','2020-12-07 16:54:21',0,0,1,NULL),('uuid2','老师(用户名)','ls',1,'123','image2','15723456541','2@qq.com','2020-12-07',0,1,'uuid1','2020-12-07 16:54:24','2020-12-07 16:57:19',0,0,1,NULL),('uuid3','学生1','xs1',0,'123','image3','15733456541','3@qq.com','2020-12-07',1,1,'uuid1','2020-12-07 16:57:15','2020-12-07 16:57:17',0,0,0,NULL),('uuid4','学生2','xs2',1,'123','image4','15723456531','4@qq.com','2020-12-07',2,1,'uuid1','2020-12-07 16:54:52','2020-12-07 16:54:52',0,0,0,NULL),('uuid5','学生3','xs3',1,'123','image5','15723451541','5@qq.com','2020-12-07',3,1,'uuid1','2020-12-07 16:55:13','2020-12-07 16:55:13',0,0,0,NULL),('uuid6','学生hw','hw',0,'123','image6','15723451531','6@qq.com','2020-12-07',1,1,'uuid1','2020-12-09 15:47:23','2020-12-09 15:47:23',0,0,0,NULL);

/*Table structure for table `exam_user_log` */

DROP TABLE IF EXISTS `exam_user_log`;

CREATE TABLE `exam_user_log` (
  `user_log_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户日志主键',
  `username` varchar(50) DEFAULT NULL COMMENT '真实姓名',
  `nickname` varchar(50) DEFAULT NULL COMMENT '昵称登录名',
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  `role_name` varchar(50) DEFAULT NULL COMMENT '角色名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户日志表';

/*Data for the table `exam_user_log` */

/*Table structure for table `exam_user_role` */

DROP TABLE IF EXISTS `exam_user_role`;

CREATE TABLE `exam_user_role` (
  `user_id` varchar(50) NOT NULL COMMENT '用户id',
  `role_id` int(11) NOT NULL COMMENT '角色id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `exam_user_role` */

insert  into `exam_user_role`(`user_id`,`role_id`) values ('uuid2',2),('uuid3',3),('uuid4',3),('uuid5',3),('uuid1',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
