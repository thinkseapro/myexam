package com.exam.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.pojo.CountExam;
import com.exam.pojo.ExerciseDetails;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 查看成绩模块<练习,考试>
 * </p>
 *
 * @author SIHAI0607
 * @since 2020-12-09
 */
@Mapper
public interface ExerciseDetailsMapper extends BaseMapper<ExerciseDetails> {
    //成绩中心（考试/练习）
    Page<ExerciseDetails> scorePage(Page<ExerciseDetails> page, @Param(Constants.WRAPPER) QueryWrapper<ExerciseDetails> qw);
    //考试中心
    Page<ExerciseDetails> examlistPage(Page<ExerciseDetails> page, @Param(Constants.WRAPPER) QueryWrapper<ExerciseDetails> qw);
    //考试场数详情
    CountExam countExam(@Param(Constants.WRAPPER) QueryWrapper<ExerciseDetails> qw);
    //首页待考试列表
    Page<ExerciseDetails> indexexam(Page<ExerciseDetails> page, @Param(Constants.WRAPPER) QueryWrapper<ExerciseDetails> qw);

}
