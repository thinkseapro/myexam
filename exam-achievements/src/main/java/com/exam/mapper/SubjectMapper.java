package com.exam.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.exam.pojo.Subject;
import org.apache.ibatis.annotations.Mapper;

//科目表
@Mapper
public interface SubjectMapper extends BaseMapper<Subject> {

}
