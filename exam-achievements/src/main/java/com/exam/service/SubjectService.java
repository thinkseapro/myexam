package com.exam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.exam.pojo.Subject;

public interface SubjectService extends IService<Subject> {
}
