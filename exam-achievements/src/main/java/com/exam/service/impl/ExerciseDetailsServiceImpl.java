package com.exam.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.exam.mapper.ExerciseDetailsMapper;
import com.exam.pojo.CountExam;
import com.exam.pojo.ExerciseDetails;
import com.exam.service.IExerciseDetailsService;
import com.exam.vo.ExerciseDetailsVo;
import com.exam.vo.MyScoreDataVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 查看成绩模块<练习,考试>
 * </p>
 *
 * @author SIHAI0607
 * @since 2020-12-09
 */
@Service
public class ExerciseDetailsServiceImpl extends ServiceImpl<ExerciseDetailsMapper, ExerciseDetails> implements IExerciseDetailsService {
    @Autowired
    private ExerciseDetailsMapper exerciseDetailsMapper;

    /**
     * 成绩中心
     * @param page
     * @param limit
     * @return
     */
    @Override
    public MyScoreDataVo<ExerciseDetails> getScoreDetails(Integer page, Integer limit,QueryWrapper<ExerciseDetails> qw) {
        MyScoreDataVo myScoreDataVo=new MyScoreDataVo();
        myScoreDataVo.setCode(0);
        myScoreDataVo.setMsg("");
        Page<ExerciseDetails> exerciseDetailsIPage=new Page<>(page,limit);
        IPage<ExerciseDetails> result=exerciseDetailsMapper.scorePage(exerciseDetailsIPage,qw);
        myScoreDataVo.setCount(result.getTotal());
        List<ExerciseDetails> exerciseDetailsList=result.getRecords();
        List<ExerciseDetailsVo> exerciseDetailsVoList=new ArrayList<>();
        for(ExerciseDetails e:exerciseDetailsList){
            ExerciseDetailsVo scoreDataVo=new ExerciseDetailsVo();
            BeanUtils.copyProperties(e,scoreDataVo);

            scoreDataVo.setSubjectName(e.getSubject().getSubjectName());
           /* //放练习名
            if(e.getPractice().getPracticeId()!=null){
                scoreDataVo.setPracticeName(e.getPractice().getPracticeName());
            }*/
            //放试卷名
            if(e.getExamination().getExaminationId()!=null){
                scoreDataVo.setExamName(e.getExamination().getExamName());
            }
            if(e.getStartTime()!=null){
                scoreDataVo.setStartTime(e.getStartTime().toString());
            }
            if(e.getDuration()!=null){
                scoreDataVo.setDuration(e.getDuration().toString());
            }
            exerciseDetailsVoList.add(scoreDataVo);
        }
        myScoreDataVo.setData(exerciseDetailsVoList);
        return myScoreDataVo;
    }

    /**
     *  考试中心
     * @param page
     * @param limit
     * @param qw
     * @return
     */
    @Override
    public MyScoreDataVo<ExerciseDetails> getExamlist(Integer page, Integer limit, QueryWrapper<ExerciseDetails> qw) {
        MyScoreDataVo myScoreDataVo=new MyScoreDataVo();
        myScoreDataVo.setCode(0);
        myScoreDataVo.setMsg("");
        Page<ExerciseDetails> exerciseDetailsIPage=new Page<>(page,limit);
        IPage<ExerciseDetails> result=exerciseDetailsMapper.examlistPage(exerciseDetailsIPage,qw);
        myScoreDataVo.setCount(result.getTotal());
        List<ExerciseDetails> exerciseDetailsList=result.getRecords();
        List<ExerciseDetailsVo> exerciseDetailsVoList=new ArrayList<>();
        for(ExerciseDetails e:exerciseDetailsList){
            ExerciseDetailsVo scoreDataVo=new ExerciseDetailsVo();
            BeanUtils.copyProperties(e,scoreDataVo);
            scoreDataVo.setSubjectName(e.getSubject().getSubjectName());
            //放试卷名
            if(e.getExamination().getExaminationId()!=null){
                scoreDataVo.setExamName(e.getExamination().getExamName());
            }
            if(e.getEndTime()!=null){
                scoreDataVo.setEndTime(e.getEndTime().toString());
            }
            scoreDataVo.setStartTime(e.getStartTime().toString());
            exerciseDetailsVoList.add(scoreDataVo);
        }
        myScoreDataVo.setData(exerciseDetailsVoList);
        return myScoreDataVo;
    }

    /**
     * 考试次数详情
     * @param qw
     * @return
     */
    @Override
    public CountExam countExam(QueryWrapper<ExerciseDetails> qw) {
        return exerciseDetailsMapper.countExam(qw);
    }

    //首页待考试列表
    @Override
    public Page<ExerciseDetails> indexexam(Page<ExerciseDetails> page, QueryWrapper<ExerciseDetails> qw) {
        return exerciseDetailsMapper.indexexam(page,qw);
    }
}
