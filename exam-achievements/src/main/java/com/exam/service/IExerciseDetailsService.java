package com.exam.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.exam.pojo.CountExam;
import com.exam.pojo.ExerciseDetails;
import com.exam.vo.MyScoreDataVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  查看成绩模块<练习,考试>
 * </p>
 *
 * @author SIHAI0607
 * @since 2020-12-09
 */
public interface IExerciseDetailsService extends IService<ExerciseDetails> {
    //成绩中心（考试/练习）
    MyScoreDataVo<ExerciseDetails> getScoreDetails(Integer page, Integer limit, QueryWrapper<ExerciseDetails> qw);
    //考试中心
    MyScoreDataVo<ExerciseDetails> getExamlist(Integer page, Integer limit, QueryWrapper<ExerciseDetails> qw);
    //考试场数详情
    CountExam countExam(QueryWrapper<ExerciseDetails> qw);
    //首页待考试列表
    Page<ExerciseDetails> indexexam(Page<ExerciseDetails> page, @Param(Constants.WRAPPER) QueryWrapper<ExerciseDetails> qw);

}
