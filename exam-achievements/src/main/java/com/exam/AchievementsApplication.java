package com.exam;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@MapperScan("com.exam.mapper")
@EnableDiscoveryClient
public class AchievementsApplication {
    public static void main(String[] args) {
        SpringApplication.run(AchievementsApplication.class,args);
    }
}
