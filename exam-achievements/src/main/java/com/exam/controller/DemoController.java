package com.exam.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.pojo.CountExam;
import com.exam.pojo.ExerciseDetails;
import com.exam.pojo.Subject;
import com.exam.service.IExerciseDetailsService;
import com.exam.service.SubjectService;
import com.exam.vo.MyScoreDataVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 考试/练习成绩详情表
 */
@RestController
@RequestMapping("/achievements")
public class DemoController {

    @Autowired  //成绩
    private IExerciseDetailsService iExerciseDetailsService;
    @Autowired  //科目
    private SubjectService subjectService;


    //成绩中心
    @RequestMapping("/scoreList")
    private MyScoreDataVo getScoreDetails(@RequestParam("page") Integer page, @RequestParam("limit") Integer limit,  @RequestBody ExerciseDetails ed){
        return iExerciseDetailsService.getScoreDetails(page,limit,
                new QueryWrapper<ExerciseDetails>()
                        .eq("flag",ed.getFlag())
                        .eq(!(ed.getSubjectId()==null ||ed.getSubjectId()==0),"ed.subject_id",ed.getSubjectId())
                        .eq(!(ed.getDetailsState()==null||ed.getDetailsState()==0),"details_state",ed.getDetailsState())
                        .eq(!(ed.getUserId()==null||ed.getUserId()==""),"ed.user_id",ed.getUserId())
        );
    }

    //科目下拉列表
    @RequestMapping("/subjectList")
    public List<Subject> getSubList(){
        List<Subject> list=subjectService.list();
        return list;
    }

    //考试中心
    @RequestMapping("/examList")
    private MyScoreDataVo getExamList(@RequestParam("page") Integer page, @RequestParam("limit") Integer limit,  @RequestBody ExerciseDetails ed){
        return iExerciseDetailsService.getExamlist(page,limit,
                new QueryWrapper<ExerciseDetails>()
                        .eq("flag",ed.getFlag())
                        .eq(!(ed.getSubjectId()==null ||ed.getSubjectId()==0),"ed.subject_id",ed.getSubjectId())
                        .eq(!(ed.getDetailsState()==null||ed.getDetailsState()==0),"details_state",ed.getDetailsState())
                        .eq(!(ed.getUserId()==null||ed.getUserId()==""),"ed.user_id",ed.getUserId())
                );
    }
    //考试次数详情
    @RequestMapping("/getCountExam")
    public CountExam getCountExam(@RequestBody ExerciseDetails ed){
        return iExerciseDetailsService.countExam(new QueryWrapper<ExerciseDetails>()
                .eq(!(ed.getFlag()==null||ed.getFlag()==0),"flag",ed.getFlag())
                .eq(!(ed.getUserId()==null||ed.getUserId()==""),"user_id",ed.getUserId())
        );
    }
    //首页显示待考试试卷
    @RequestMapping("/indexExam")
    public Page<ExerciseDetails> indexexam(@RequestParam String uid){
        return iExerciseDetailsService.indexexam(new Page(1,3),new QueryWrapper<ExerciseDetails>()
                .eq("flag",1)
                .eq("eed.user_id",uid)
                .ne("details_state",1)
                .orderByDesc("start_time"));
    }
    @RequestMapping("/insertExerciseDetails")
    public boolean insertExerciseDetails(@RequestBody ExerciseDetails exerciseDetails){
        boolean num=iExerciseDetailsService.save(exerciseDetails);
        return num;
    }
}
