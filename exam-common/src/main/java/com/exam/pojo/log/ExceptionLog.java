package com.exam.pojo.log;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 异常返回信息
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExceptionLog {

    private String params;//请求参数
    private String keys;//返回结果
    private String Ip; //ip
    private String Uri;//url
    private String Ver;//操作版本
    private String Method;//方法名
    private String ExcName;//异常名称
    private String ExcMessage;//异常信息
    private Date CreateTime;//发送异常时间
}
