package com.exam.pojo.log;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 正常通知
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OperaLog {
    private String ID;// id
    private String Method;//方法
    private String params;// 请求参数
    private String keys;// 返回结果
    private String Ip; // ip
    private String Uri;// url
    private String Ver;// 操作版本
    private String Modul;// 操作模块
    private String Type;// 操作类型
    private String Desc;// 描述
    private Date CreateTime;//创建时间




}
