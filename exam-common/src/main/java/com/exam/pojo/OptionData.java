package com.exam.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 选项内容表
 * </p>
 *
 * @author huwei
 * @since 2020-12-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("exam_option_data")
@ApiModel(value="OptionData对象", description="选项内容表")
public class OptionData implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "option_id", type = IdType.AUTO)
    private Integer optionId;

    @ApiModelProperty(value = "标识A-0、B-1、C-2")
    private char optionFlag;

    @ApiModelProperty(value = "选项内容")
    private String optionName;

    @ApiModelProperty(value = "题目id")
    private Integer topicId;

    @ApiModelProperty(value = "该选项是否为正确答案--0:错误 1:正确")
    private Integer isAnswer;


}
