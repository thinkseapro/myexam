package com.exam.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author huwei
 * @since 2020-12-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("exam_topic_practice")
@ApiModel(value="TopicPractice对象", description="")
public class TopicPractice implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "题目+练习中间表id")
    @TableId(value = "tp_id", type = IdType.AUTO)
    private Integer tpId;

    @ApiModelProperty(value = "题目主键")
    private Integer topicId;

    @ApiModelProperty(value = "练习主键")
    private Integer practiceId;


}
