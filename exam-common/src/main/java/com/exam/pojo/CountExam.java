package com.exam.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * @author 霸霸
 *  @since 2020-12-14
 *  格外添加的类，用于查询考试次数详情
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CountExam {
    //总考试次数
    @TableField(exist = false)
    private Integer countExam;

    //及格次数
    @TableField(exist = false)
    private Integer countPassExam;

    //不及格次数
    @TableField(exist = false)
    private Integer countFailExam;

    //待考试场数
    @TableField(exist = false)
    private Integer countWaitExam;




}
