package com.exam.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 霸霸
 * @since 2020-12-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("user_enterprise")
@ApiModel(value="UserEnterprise对象", description="")
public class UserEnterprise implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户-试题表主键")
    @TableId(value = "ut_id", type = IdType.AUTO)
    private Integer utId;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "企业试卷详情id")
    private Integer enterpriseDetailsId;


}
