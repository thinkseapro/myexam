package com.exam.pojo;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 企业试卷详情表
 * </p>
 *
 * @author huwei
 * @since 2020-12-14
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("exam_enterprise_details")
@ApiModel(value="EnterpriseDetails对象", description="企业试卷详情表")
public class EnterpriseDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(value = "enterprise_details_id", type = IdType.AUTO)
    private Integer enterpriseDetailsId;

    @ApiModelProperty(value = "试卷主键")
    private Integer examinationId;

    @ApiModelProperty(value = "价格")
    private Float enterpriseDetailsPrice;

    @ApiModelProperty(value="优惠价格")
    private Float enterpriseDetailsDiscount;

    @ApiModelProperty(value = "已售出数量")
    private Integer enterpriseDetailsBuyNum;

    @ApiModelProperty(value = "限量数")
    private Integer enterpriseDetailsLimitNum;

    @ApiModelProperty(value = "0:限量优惠试题，1:经典试题")
    private Integer enterpriseDetailsType;

    @TableField(exist = false)
    @ApiModelProperty(value = "试卷表")
    private Examination examination;

    @TableField(exist = false)
    @ApiModelProperty(value = "科目表")
    private Subject subject;

    @TableField(exist = false)
    @ApiModelProperty(value="已购试题表")
    private UserEnterprise userEnterprise;
}
