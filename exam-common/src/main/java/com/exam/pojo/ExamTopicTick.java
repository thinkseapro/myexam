package com.exam.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 错题反馈表
 * </p>
 *
 * @author author
 * @since 2020-12-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("exam_topic_tick")
@ApiModel(value="ExamTopicTick对象", description="错题反馈表")
public class ExamTopicTick implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "错题反馈id")
    @TableId(value = "topic_tick_id", type = IdType.AUTO)
    private Integer topicTickId;

    @ApiModelProperty(value = "题目id")
    private Integer topicId;

    @ApiModelProperty(value = "用户主键")
    private String userId;

    @ApiModelProperty(value = "题目名称")
    private String topicName;

    @ApiModelProperty(value = "反馈信息")
    private String topicTickCommon;

    @ApiModelProperty(value = "修改的状态--0:未修改 1:已修改")
    private Integer topicTickState;

    @ApiModelProperty(value = "创建时间")
    private Date topicTickTime;

}
