package com.exam.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.time.LocalDate;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 普通用户表
 * </p>
 *
 * @author huwei
 * @since 2020-12-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("exam_user")
@ApiModel(value="User对象", description="普通用户表")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户主键")
    @TableId(value = "user_id", type = IdType.AUTO)
    private String userId;

    @ApiModelProperty(value = "真实姓名")
    private String username;

    @ApiModelProperty(value = "昵称(用于登录,唯一)")
    private String nickname;

    @ApiModelProperty(value = "性别,0男,1女")
    private Integer sex;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "用户头像地址")
    private String userImage;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "生日")
    private LocalDate birth;

    @ApiModelProperty(value = "'学生'所属班级id(0代表该用户不是学生)")
    private Integer classId;

    @ApiModelProperty(value = "是否审核通过0未审核,1已审核")
    private Integer isAudit;

    @ApiModelProperty(value = "审核者")
    private String auditUserId;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime modifyTime;

    @ApiModelProperty(value = "状态,0未锁定, 1禁用锁定(注意,只有审核通过和该账户没被锁定才能登陆)-->后台有个是否禁用的按钮")
    private Integer isLock;

    @ApiModelProperty(value = "是否删除,0未删除,1已删除")
    private Integer isDelete;

    @ApiModelProperty(value = "是否为管理员(0不是;1是的,也标识是最高权限拥有者;3表示被管理员赋予进入后台登录资格标识)==>(1整个系统中只能有一个,能进入后台管理的只能是管理员赋予的资格)")
    private Integer isUserManager;

    @ApiModelProperty(value = "最后一次登录时间")
    private LocalDate lastLogin;

    @ApiModelProperty(value = "盐(uuid)")
    private String salt;

    @ApiModelProperty(value = "判断原先角色是不是学生(0是学生,1不是学生)")
    private Integer isStudent;

    @ApiModelProperty(value = "用户是否重置密码(0:未重置，1:已重置)")
    private Integer isReset;


}
