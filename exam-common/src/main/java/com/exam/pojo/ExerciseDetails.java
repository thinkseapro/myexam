package com.exam.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.time.LocalTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 考试|练习 详情表
 * </p>
 *
 * @author huwei
 * @since 2020-12-08
 */
@Data
@TableName("exam_exercise_details")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ExerciseDetails对象", description="考试|练习 详情表")
@AllArgsConstructor
@NoArgsConstructor
public class ExerciseDetails implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "考试|练习 详情id")
    @TableId(value = "exam_exercise_details_id", type = IdType.AUTO)
    private Integer examExerciseDetailsId;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @JsonFormat(pattern = "HH:mm:ss")
    @ApiModelProperty(value = "用时时长")
    private LocalTime duration;

    @ApiModelProperty(value = "状态--0:练习中 1:已提交")
    private Integer detailsState;

    @ApiModelProperty(value = "分数")
    private String detailsScore;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "开始时间")
    private LocalDateTime startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "提交时间")
    private LocalDateTime endTime;

    @ApiModelProperty(value = "科目id")
    private Integer subjectId;

    @ApiModelProperty(value = "0:练习详情,1考试详情")
    private Integer flag;

    @ApiModelProperty(value = "<练习:为空,考试:试卷主键>")
    private Integer examinationId;

    @ApiModelProperty(value = "练习id")
    private Integer practiceId;

    @ApiModelProperty(value = "作弊(是否异常<练习:为空,考试:0异常||1正常>)")
    private Integer isException;

    @TableField(exist = false)
    @ApiModelProperty(value = "科目表")
    private Subject subject;

    @TableField(exist = false)
    @ApiModelProperty(value = "试卷表")
    private Examination examination;

    @TableField(exist = false)
    @ApiModelProperty(value="用户表")
    private User user;
}
