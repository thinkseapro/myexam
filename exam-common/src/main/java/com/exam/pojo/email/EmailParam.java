package com.exam.pojo.email;

import lombok.Data;

/**
 * 邮件参数实体类
 * @author 
 *
 */
@Data
public class EmailParam {
	private String title;//标题
	private String password;//默认密码
	private String token;//token
	private String email;//收件方邮箱
	private String imagePath;//图标
}