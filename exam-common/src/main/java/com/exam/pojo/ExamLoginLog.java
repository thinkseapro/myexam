package com.exam.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户日志表
 * </p>
 *
 * @author SIHAI0607
 * @since 2020-12-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("exam_login_log")
@ApiModel(value="ExamLoginLog对象", description="用户日志表")
public class ExamLoginLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户日志主键")
    @TableId(value = "login_log_id", type = IdType.AUTO)
    private Integer loginLogId;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "真实姓名")
    private String username;

    @ApiModelProperty(value = "昵称登录名")
    private String nickname;

    @ApiModelProperty(value = "登录时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty(value = "IP地址")
    private String ip;

    @ApiModelProperty(value = "登录地点")
    private String location;

    @ApiModelProperty(value = "操作系统")
    private String userSystem;

    @ApiModelProperty(value = "浏览器")
    private String userBrowser;


}
