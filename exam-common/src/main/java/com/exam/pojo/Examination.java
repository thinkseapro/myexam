package com.exam.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.time.LocalTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 试卷表
 * </p>
 *
 * @author huwei
 * @since 2020-12-14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("exam_examination")
@ApiModel(value="Examination对象", description="试卷表")
public class Examination implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "试卷主键")
    @TableId(value = "examination_id", type = IdType.AUTO)
    private Integer examinationId;

    @ApiModelProperty(value = "试卷名称")
    private String examName;

    @ApiModelProperty(value = "出卷人id")
    private String userId;

    @ApiModelProperty(value = "科目id")
    private Integer subjectId;

    @ApiModelProperty(value = "班级id")
    private Integer classId;

    @JsonFormat(pattern = "HH:mm:ss")
    @ApiModelProperty(value = "时长")
    private LocalTime duration;

    @ApiModelProperty(value = "总分")
    private String examinationTotalScore;

    @ApiModelProperty(value = "题数")
    private Integer topicSum;

    /*@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "开始时间")
    private LocalDateTime startTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "结束时间")
    private LocalDateTime endTime;*/

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "描述")
    private String common;

    @ApiModelProperty(value = "0:本校试卷,1:企业试卷")
    private Integer examinationFlag;

    @ApiModelProperty(value = "试卷封面")
    private String examinationImg;


}
