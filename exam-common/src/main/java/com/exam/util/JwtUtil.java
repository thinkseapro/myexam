package com.exam.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * JWT工具类
 **/
@Component
public class JwtUtil {

    private static final String key="BDHHJBHCB73AH0YB"; //密钥加盐

    private static final long ttl=3600000;   //过期时间

    /**
     * 生成邮箱JWT
     */
    public static String createMailJWT(String mail) {
        // 设置过期时间
        Date date = new Date(System.currentTimeMillis() + 901);
        // 私钥和加密算法
        Algorithm algorithm = Algorithm.HMAC256(key);
        // 设置头部信息
        Map<String, Object> header = new HashMap<>(1);
        header.put("Type", "Jwt");
        // 返回token字符串
        return JWT.create()
                .withHeader(header)
                .withClaim("mail", mail)
                .withExpiresAt(date)
                .sign(algorithm);
    }

    /**
     * 从token中获取邮箱信息
     * @param **token**
     * @return
     */
    public static String getMail(String token){
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim("mail").asString();
        } catch (JWTDecodeException e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 生成用户JWT
     */
    public static String createJWT(String userid, String nickname,String username) {
        // 设置过期时间
        Date date = new Date(System.currentTimeMillis() + ttl);
        // 私钥和加密算法
        Algorithm algorithm = Algorithm.HMAC256(key);
        // 设置头部信息
        Map<String, Object> header = new HashMap<>(1);
        header.put("Type", "Jwt");
        // 返回token字符串
        String token= JWT.create()
                .withHeader(header)
                .withClaim("nickname", nickname)
                .withClaim("userid",userid)
                .withClaim("username",username)
                .withExpiresAt(date)
                .sign(algorithm);
        return token;
    }

    /**
     * 获取Token
     * @return
     */
     public static String GetToken(){
         HttpServletRequest request = ((ServletRequestAttributes) (RequestContextHolder.currentRequestAttributes())).getRequest();
         String token=request.getHeader("Exam-Authorization");
         System.out.println("获取到的头部信息:" + request.getHeader("Exam-Authorization"));
         if (CommonUtil.isEmpty(token)) return "";
         return token;
     }



    /**
     * 检验token是否正确
     * @param **token**
     * @return
     */
    public static boolean verify(String token){
           try {
               Algorithm algorithm = Algorithm.HMAC256(key);
               JWTVerifier verifier = JWT.require(algorithm).build();
               DecodedJWT jwt = verifier.verify(token);
           }catch (Exception e){

           }
            return true;
    }


    /**
     * 从token中获取nickname信息
     * @param **token**
     * @return
     */
    public static String getNickname(String token){
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim("nickname").asString();
        } catch (JWTDecodeException e){
            e.printStackTrace();
            return null;
        }
    }


    /**
     * 从token中获取用户id
     * @param **token**
     * @return
     */
    public static String getUserId(String token){
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim("userid").asString();
        } catch (JWTDecodeException e){
            e.printStackTrace();
            return null;
        }
    }


}