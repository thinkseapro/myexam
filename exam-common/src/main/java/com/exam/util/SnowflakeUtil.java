package com.exam.util;


import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.net.NetUtil;
import cn.hutool.core.util.IdUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Slf4j
@Component
public class SnowflakeUtil {

    private long workedId = 0;
    private long datacenterId=1;
    private Snowflake snowflake=IdUtil.createSnowflake(workedId,datacenterId);

    @PostConstruct
    public void init(){
        try {
            workedId= NetUtil.ipv4ToLong(NetUtil.getLocalhostStr());
            log.info("当前机器的workId:{}",workedId);
        }catch (Exception e){
            e.printStackTrace();
            log.warn("当前机器的workId:{}获取失败 "+e);
            workedId=NetUtil.getLocalhostStr().hashCode();
        }
    }

    /**
     *  生成随机数字
     * @return
     */
    public synchronized  long snowflakeId(){
        return snowflake.nextId();
    }

    /**
     * 自增ID,根据机器号和开始位置
     * @param workedId  机器号
     * @param datacenterId ID+1
     * @return
     */
    public synchronized  long snowflakeId(long workedId,long datacenterId){
        Snowflake snowflake=IdUtil.createSnowflake(workedId,datacenterId);
        return snowflake.nextId();
    }

    /**
     * 获取UUID
     * @return
     */
    public synchronized String uuid(){
        String snowflake1=IdUtil.randomUUID();
        return snowflake1;
    }



}
