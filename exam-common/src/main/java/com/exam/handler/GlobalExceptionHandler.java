package com.exam.handler;

import com.exam.constant.ExamCode;
import com.exam.constant.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author NieChangan
 */
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    /**
     * 全局异常处理,没有指定异常的类型,不管什么异常都是可以捕获的
     *
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result error(Exception e) {
        e.printStackTrace();
        log.error(e.getMessage());
        return Result.error()
                .code(7777)
                .message("已被全局异常捕获");
    }

    /**
     * 处理特定异常类型,可以定义多个,ARITHMETIC_EXCEPTION为例
     *
     * @param e
     * @return
     */
    @ExceptionHandler(ArithmeticException.class)
    @ResponseBody
    public Result error(ArithmeticException e) {
        e.printStackTrace();
        log.error(e.getMessage());
        return Result
                .error()
                .code(ExamCode.EXAM_EXCEPTION_ARITHMETIC.getCode())
                .message(ExamCode.EXAM_EXCEPTION_ARITHMETIC.getMessage());
    }
    /**
     * 处理特定异常类型,可以定义多个,NullPointerException为例
     *
     * @param e
     * @return
     */
    @ExceptionHandler(NullPointerException.class)
    @ResponseBody
    public Result error(NullPointerException e) {
        e.printStackTrace();
        log.error(e.getMessage());
        return Result
                .error()
                .code(ExamCode.Exam_EXCEPTION_POINTER_NULL.getCode())
                .message(ExamCode.Exam_EXCEPTION_POINTER_NULL.getMessage());
    }


    /**
     * 处理业务异常,我们自己定义的异常
     *
     * @param e
     * @return
     */
    @ExceptionHandler(BusinessException.class)
    @ResponseBody
    public Result error(BusinessException e) {
        e.printStackTrace();
        log.error(e.getErrMsg());
        return Result.error().code(e.getCode())
                .message(e.getErrMsg());
    }

}
