package com.exam.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 *<p>试卷提交vo类<p/>
 *
 * @author loop
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("exam_exercise_topic")
@ApiModel(value="ExamExerciseTopic对象", description="")
public class ExamExerciseDeatilsVo {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "(考试|练习 题目id)")
    @TableId(value = "exam_exercise_topic_id", type = IdType.AUTO)
    private Integer examExerciseTopicId;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "题目id")
    private Integer topicId;

    @ApiModelProperty(value = "正确答案")
    private String correctAnswer;

    @ApiModelProperty(value = "作答答案--A0、B1、C2")
    private String exerciseAnswer;

    @ApiModelProperty(value = "(考试|练习 详情id)")
    private Integer examExerciseDetailsId;

    @ApiModelProperty(value = "答题是否正确 1true/0false")
    private Integer isture;

}
