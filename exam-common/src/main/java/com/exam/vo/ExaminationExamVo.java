package com.exam.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

/**
 * 试卷vo类
 * @author loop
 */
@SuppressWarnings("ALL")
@Data
@ApiModel(value="ExaminationVo对象", description="考试Vo类")
public class ExaminationExamVo implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "试卷主键")
    @TableId(value = "examination_id", type = IdType.AUTO)
    private Integer examinationId;

    @ApiModelProperty(value = "考试详情编号")
    private Integer examExerciseDetailsId;

    @ApiModelProperty(value = "用户编号")
    private String userId;

    @ApiModelProperty(value = "试卷名称")
    private String examName;

    @ApiModelProperty(value = "时长")
    private LocalTime duration;

    @ApiModelProperty(value = "总分")
    private String examinationTotalScore;

    @ApiModelProperty(value = "题数")
    private Integer topicSum;

    @TableField(exist = false)
    @ApiModelProperty(value = "题目集合")
    private List<TopicVo> tList;

    @ApiModelProperty("答题记录集合")
    private List<ExamExerciseTopicVo> recordList;


}
