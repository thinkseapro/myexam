package com.exam.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 选项vo类
 * @author loop
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Option对象", description="选项表")
public class OptionVo implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "option_id", type = IdType.AUTO)
    private Integer optionId;

    @ApiModelProperty(value = "标识A-0、B-1、C-2")
    private String optionFlag;


    @ApiModelProperty(value = "选项内容")
    private String optionName;

    @TableField(exist = false)
    @ApiModelProperty(value = "用户选择答案")
    private String respond;

}
