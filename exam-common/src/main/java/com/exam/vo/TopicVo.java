package com.exam.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 题目Vo类
 *
 * @author loop
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "Topic对象", description = "题目表")
public class TopicVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "题目主键")
    @TableId(value = "topic_id", type = IdType.AUTO)
    private Integer topicId;

    @ApiModelProperty(value = "题目名称")
    private String topicTitle;

    @ApiModelProperty(value = "该题分数")
    private String topicScore;

    @ApiModelProperty(value = "错题次数")
    private Integer ts;

    @ApiModelProperty(value = "题目类型编号")
    private Integer typeId;

    @ApiModelProperty(value = "题目类型名称")
    private String typeName;

    @ApiModelProperty(value = "正确答案")
    private String topicResult;

    @ApiModelProperty(value = "题目解析")
    private  String parse;


    @TableField(exist = false)
    @ApiModelProperty(value = "用户选择答案")
    private String respond;

    @TableField(exist = false)
    @ApiModelProperty(value = "题目选项集合")
    List<OptionVo> oList;

    @TableField(exist = false)
    @ApiModelProperty(value = "题目难度等级名称")
    private String levelName;

    @TableField(exist = false)
    @ApiModelProperty(value = "题目难度等级")
    private String levelId;

    @TableField(exist = false)
    @ApiModelProperty(value = "答题记录编号")
    private Integer exerciseId;
}
