package com.exam.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.exam.pojo.OptionData;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

/**
 *<p>错题vo类<p/>
 *
 * @author loop
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)

@TableName("exam_exercise_topic")
@ApiModel(value="ExamExerciseTopic对象", description="")
public class ExamExerciseTopicErrorVo {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "题目id")
    private Integer topicId;

    @ApiModelProperty(value = "题目名称")
    private String topicTitle;

    @ApiModelProperty(value = "选项集合")
    private List<OptionData> optionData;

    @ApiModelProperty(value = "正确答案")
    private String correctAnswer;

    @TableField(exist = false)
    @ApiModelProperty(value = "作答答案--A0、B1、C2")
    private String exerciseAnswer;

    @ApiModelProperty(value = "科目主键")
    private Integer subjectId;

    @ApiModelProperty(value = "科目名称")
    private String subjectName;

    @ApiModelProperty(value = "开始时间")
    private LocalDateTime startTime;

    @ApiModelProperty(value = "答案解析")
    private String parse;

    /*--------------请求参数----------------*/

    @ApiModelProperty(value = "时间排序： 1正序(离我最近)，2倒序(离我最远)")
    private Integer timeNum;

    @ApiModelProperty(value = "模式: 0练习模式，1考试模式")
    private Integer mode;

    @ApiModelProperty(value = "从第n行开始查询--分页")
    private Integer startpage;

    @ApiModelProperty(value = "查询n条数据--分页")
    private Integer pagesize;

}
