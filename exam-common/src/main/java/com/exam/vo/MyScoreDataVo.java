package com.exam.vo;

import lombok.Data;

import java.util.List;

/**
 * 成绩中心layui表格所需要的数据格式
 */
@Data
public class MyScoreDataVo<T> {
    private Integer code;
    private String msg;
    private Long count;
    private List<T> data;
}
