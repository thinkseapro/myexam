package com.exam.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * 用户-班级VO
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserVo implements Serializable {

    private String userId;
    private String username;
    private String nickname;
    private Integer sex;
    private String userImage;
    private String phone;
    private String email;
    private LocalDate lastLogin;
    //班级
    private Integer classId;
    private String className;
    //年级
    private Integer gradeId;
    private String gradeName;


}
