package com.exam.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 企业试卷订单表
 * </p>
 *
 * @author huwei
 * @since 2020-12-14
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("exam_enterprise_orders")
@ApiModel(value="EnterpriseOrders对象", description="企业试卷订单表")
public class EnterpriseOrdersVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "订单id")
    @TableId(value = "enterprise_orders_id", type = IdType.AUTO)
    private Integer enterpriseOrdersId;

    @ApiModelProperty(value = "订单编号")
    private Long enterpriseOrdersNumber;

    @ApiModelProperty(value = "企业试卷详情主键id")
    private Integer enterpriseDetailsId;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "订单价格")
    private Float enterpriseOrdersPrice;

    @ApiModelProperty(value = "是否付款 0:未付款 1:已付款")
    private Integer ispay;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "订单创建时间")
    private LocalDateTime enterpriseOrdersCreatetime;


}
