package com.exam.constant;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

/**
 * 公共返回结果
 */
@Data
@AllArgsConstructor
public class Result {

    @ApiModelProperty(value = "返回数据")
    private Map<String,Object> data = new HashMap<>();

    @ApiModelProperty(value = "是否成功")
    private Boolean success;

    @ApiModelProperty(value = "返回码")
    private Integer code;

    @ApiModelProperty(value = "返回消息")
    private String message;


    /**
     * 构造方法私有化,里面的方法都是静态方法
     * 达到保护属性的作用
     */
    private Result(){

    }

    /**
     * 这里是使用链式编程
     */
    public static Result error(){
        Result result = new Result();
        result.setSuccess(false);
        result.setCode(ExamCode.Exam_FAIL.getCode());
        result.setMessage(ExamCode.Exam_FAIL.getMessage());
        return result;
    }
    public static Result error(ExamCode examCode){
        Result result = new Result();
        result.setSuccess(false);
        result.setCode(examCode.Exam_FAIL.getCode());
        result.setMessage(examCode.Exam_FAIL.getMessage());
        return result;
    }


    public static Result result(ExamCode examCode){
       Result result=new Result();
       result.setCode(examCode.getCode());
        result.setSuccess(examCode.getSuccess());
       result.setMessage(examCode.getMessage());
        return result;
    }

    public static Result result(ExamCode examCode,Map<String,Object> map){
        Result result=new Result();
        result.setCode(examCode.getCode());
        result.setSuccess(examCode.getSuccess());
        result.setMessage(examCode.getMessage());
        result.setData(map);
        return result;
    }

    public static Result result(ExamCode examCode,String key,Object value){
        Result result=new Result();
        result.setCode(examCode.getCode());
        result.setSuccess(examCode.getSuccess());
        result.setMessage(examCode.getMessage());
        Map<String,Object> data = new HashMap<>();
        data.put(key,value);
        result.setData(data);
        return result;
    }

    /**
     * 自定义返回成功与否
     * @param success
     * @return
     */
    public Result success(Boolean success){
        this.setSuccess(success);
        return this;
    }

    public Result message(String message){
        this.setMessage(message);
        return this;
    }

    public Result code(Integer code){
        this.setCode(code);
        return this;
    }

    public Result data(String key,Object value){
        this.data.put(key,value);
        return this;
    }

    public Result data(Map<String,Object> map){
        this.setData(map);
        return this;
    }



//    public static void main(String[] args) {
//        System.out.println(Result.error());
//    }
}
