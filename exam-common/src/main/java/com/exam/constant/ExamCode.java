package com.exam.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * 状态码
 * @author Think
 */

public enum ExamCode implements CustomizeExamCode {

    //数字一
    EXAM_CODE_ONE(true,1,"数字一"),

    //超过访问次数
    EXAM_ACCESS_LIMIT_REACHED(false,560,"超过访问次数"),

    //全局异常
    EXAM_EXCEPTION_ALL(false,500,"全局异常"),
    //算术异常
    EXAM_EXCEPTION_ARITHMETIC(false,515,"算术异常"),
    //空指针异常
    Exam_EXCEPTION_POINTER_NULL(false,516,"空指针异常"),

    //正确
    Exam_SUCCESS(true, 200, "正确"),
    //错误
    Exam_FAIL(false, 400, "错误"),
    //为空
    Exam_ISNULL(false, 411, "为空"),
    //密码修改成功
    Exam_UPDATE_password_SUCCESS(true,410,"密码修改成功"),
    //该用户已登录
    EXAM_LOGIN_EXIST(false,418,"您已登录，请不要重复登录哦~"),
    //密码修改失败
    Exam_UPDATE_password_error(false,419,"异常操作,密码修改失败"),
    //密码不正确
    Exam_Login_password_error(false,420,"密码错误"),
    //用户名不存在
    Exam_Login_username_error(false,421,"用户名不存在"),
    //密码已重置
    Exam_Login_PASSWORD_RESET(true,428,"密码已重置"),
    //正确
    Exam_Login_SUCCESS(true, 220, "登录成功"),
    //登录失效
    Exam_Login_error(false, 422, "登录失效"),
    //邮箱发送次数已满
    EXAM_EMAIL_MEET(false,423,"今日邮箱验证次数已达上限<br>请明天再试！"),
    //验证码发送异常
    EXAM_PHONE_SEND_ERROR(false,424,"验证码发送异常"),
    //验证码发送成功
    EXAM_PHONE_SEND_SUCCESS(true,430,"验证码发送成功"),
    //手机号不存在
    EXAM_PHONE_ISNULL(false,425,"手机号不存在"),
    //手机验证码未过期
    EXAM_PHONE_EXIST(false,426,"手机验证码未过期"),
    //验证码错误
    EXAM_PHONE_CODE_ERROR(false,427,"验证码错误"),
    //验证码正确
    EXAM_PHONE_CODE_SUCCESS(true,440,"验证码正确"),
    //邮箱不存在
    EXAM_EMAIL_NOT_EXIST(false,441,"邮箱不存在"),
    //已过期
    EXAM_Expired(false,442,"已过期,请重新发送验证码"),
    //邮箱验证码错误
    EXAM_EMAIL_CODE_ERROR(false,443,"验证码错误"),
    //发送异常
    Exam_SEND_ERROR(false,601,"发送异常"),
    //付款
    Exam_MONEY_PAID(true, 1, "付款"),
    //订单已确认
    Exam_ORDER_CONFIRM(true, 1, "订单已经确认"),
    //订单已取消
    Exam_ORDER_CANCEL(false, 2, "订单已取消"),
    //订单已取消
    Exam_ORDER_INVALID(false, 3, "订单无效"),
    //订单已付款
    Exam_ORDER_PAY_STATUS_NO_PAY(true,0,"订单未付款"),
    //订单已付款
    Exam_ORDER_PAY_STATUS_PAYING(true,1,"订单正在付款"),
    //订单已付款
    Exam_ORDER_PAY_STATUS_IS_PAY(true,2,"订单已付款"),
    //请求参数有误
    Exam_REQUEST_PARAMETER_VALID(false, -1, "请求参数有误"),
    //订单总价格不合法
    Exam_ORDERAMOUNT_INVALID(false, 10003, "订单总价格不正确"),
    //订单保存失败
    Exam_ORDER_SAVE_ERROR(false, 10004, "订单保存失败"),
    //订单确认失败
    Exam_ORDER_CONFIRM_FAIL(false, 10005, "订单确认失败"),
    //商品不存在
    Exam_GOODS_NO_EXIST(false, 20001, "商品不存在"),
    //订单价格非法
    Exam_GOODS_PRICE_INVALID(false, 20002, "商品价格非法"),
    //商品库存不足
    Exam_GOODS_NUM_NOT_ENOUGH(false, 20003, "商品库存不足"),
    //扣减库存失败
    Exam_REDUCE_GOODS_NUM_FAIL(false, 20004, "扣减库存失败"),
    //库存记录为空
    Exam_REDUCE_GOODS_NUM_EMPTY(false, 20005, "扣减库存失败"),
    //用户账号不能为空
    Exam_USER_IS_NULL(false, 30001, "用户账号不能为空"),
    //用户信息不存在
    Exam_USER_NO_EXIST(false, 30002, "用户不存在"),
    //支付订单未找到
    Exam_PAYMENT_NOT_FOUND(false,70001,"支付订单未找到"),
    //支付订单已支付
    Exam_PAYMENT_IS_PAID(false,70002,"支付订单已支付"),
    //订单付款失败
    Exam_PAYMENT_PAY_ERROR(false,70002,"订单支付失败");


    Boolean success;
    Integer code;
    String message;

    ExamCode() {

    }

    ExamCode(Boolean success, Integer code, String message) {
        this.success = success;
        this.code = code;
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    @Override
    public String toString() {
        return "ExamCode{" +
                "success=" + success +
                ", code=" + code +
                ", message='" + message + '\'' +
                '}';
    }
}
