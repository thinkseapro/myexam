package com.exam.constant;

/**
 * 自定义处理结果
 */
public interface CustomizeExamCode {
    /**
     * 获取错误状态码
     * @return
     */
    Integer getCode();

    /**
     * 获取错误信息
     * @return
     */
    String getMessage();

    /**
     * 获取是否状态
     * @return
     */
    Boolean getSuccess();

}
