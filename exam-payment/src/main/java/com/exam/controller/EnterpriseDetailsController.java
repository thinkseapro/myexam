package com.exam.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.pojo.EnterpriseDetails;
import com.exam.pojo.EnterpriseOrders;
import com.exam.pojo.UserEnterprise;
import com.exam.service.EnterpriseDetailsService;
import com.exam.service.EnterpriseOrdersService;
import com.exam.service.IUserEnterpriseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 企业试卷详情
 */
@RestController
@RequestMapping("/payment")
public class EnterpriseDetailsController {
    @Autowired
    private EnterpriseDetailsService enterpriseDetailsService;
    @Autowired
    private EnterpriseOrdersService enterpriseOrdersService;
    @Autowired
    private IUserEnterpriseService iUserEnterpriseService;

    //查询企业试卷
    @RequestMapping("/getenterpriseList/{current}/{size}")
    public Page<EnterpriseDetails> getenterpriseList(@RequestBody EnterpriseDetails ed, @PathVariable(value="current") int current, @PathVariable(value="size") int size){
         Page<EnterpriseDetails> ep=enterpriseDetailsService.enterpriseList(new Page<>(current,size),
                 new QueryWrapper<EnterpriseDetails>()
                         .notInSql("ed.enterprise_details_id","SELECT enterprise_details_id FROM user_enterprise WHERE user_id='uuid6'")
                         .eq(!(ed.getEnterpriseDetailsType()==null),
                                 "enterprise_details_type",ed.getEnterpriseDetailsType())
                         .eq(!(ed.getExaminationId()==null || ed.getExaminationId()==0),"ed.examination_id",ed.getExaminationId())
         );
         return ep;
    }

    //新增企业试卷的订单
    @RequestMapping("/insertEnterOrder")
    public boolean insertEnterOrder(@RequestBody EnterpriseOrders enterpriseOrders){
        boolean flag=enterpriseOrdersService.save(enterpriseOrders);
        return flag;
    }
    //新增用户-企业试卷中间表
    @RequestMapping("/insertUserEnterprise")
    public boolean insertUserEnterprise(@RequestBody UserEnterprise userEnterprise){
        boolean flag=iUserEnterpriseService.save(userEnterprise);
        return flag;
    }
    //修改企业试卷详情表
    @RequestMapping("/upEnterDetails")
    public boolean updateEnterDetails(@RequestBody EnterpriseDetails enterpriseDetails){
        UpdateWrapper<EnterpriseDetails> updateWrapper=new UpdateWrapper<>();
        updateWrapper.eq("enterprise_details_id",enterpriseDetails.getEnterpriseDetailsId());
        updateWrapper.set("enterprise_details_buy_num",enterpriseDetails.getEnterpriseDetailsBuyNum());
        return enterpriseDetailsService.update(updateWrapper);
    }
    //查看订单
    @RequestMapping("/enterOrderPage/{enterTypeid}")
    public Page<EnterpriseOrders> enterOrderPage(@PathVariable(value = "enterTypeid") Integer enterTypeid){
         return enterpriseOrdersService.selectEnterOrder(new Page<>(),new QueryWrapper<EnterpriseOrders>()
                 .eq(!(enterTypeid==null),
                         "enterprise_details_type",enterTypeid).eq("ispay",1)
         );
    }
    //显示首页企业试卷列表
    @RequestMapping("/indexEnterpriseDetails")
    public Page<EnterpriseDetails> indexEnterpriseDetails(@RequestBody EnterpriseDetails ed){
         return  enterpriseDetailsService.indexenterprise(new Page<>(1,50),new QueryWrapper<>());
    }
    //根据订单号查看订单
    @RequestMapping("/getOneByOrdersNumber")
    public EnterpriseOrders getOneByOrdersNumber(@RequestParam long oid){
        EnterpriseOrders one=enterpriseOrdersService.getOne(new QueryWrapper<EnterpriseOrders>().eq("enterprise_orders_number",oid));
        return one;
    }
    //修改订单状态为已支付
    @RequestMapping("/updateEnterOrders")
    public boolean updateEnterOrders(@RequestParam long oid){
        UpdateWrapper<EnterpriseOrders> updateWrapper=new UpdateWrapper<>();
        updateWrapper.eq("enterprise_orders_number",oid);
        updateWrapper.set("ispay",1);
        return enterpriseOrdersService.update(updateWrapper);
    }
    //根据企业详情id查询试卷
    @RequestMapping("/getOneByDetailsId")
    public EnterpriseDetails getOneByDetailsId(@RequestParam int did){
        EnterpriseDetails one=enterpriseDetailsService.getOne(new QueryWrapper<EnterpriseDetails>()
        .eq("enterprise_details_id",did));
        return one;
    }
}
