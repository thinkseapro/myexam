package com.exam.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import com.exam.pojo.EnterpriseDetails;
import org.apache.ibatis.annotations.Param;

/**
 * 企业试卷详情
 */
public interface EnterpriseDetailsService extends IService<EnterpriseDetails> {
    //查询企业试卷
    Page<EnterpriseDetails> enterpriseList(Page<EnterpriseDetails> page, QueryWrapper<EnterpriseDetails> qw);
    //显示首页企业试卷列表
    Page<EnterpriseDetails> indexenterprise(Page<EnterpriseDetails> page, @Param(Constants.WRAPPER) QueryWrapper<EnterpriseDetails> qw);

}
