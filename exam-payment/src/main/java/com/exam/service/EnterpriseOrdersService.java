package com.exam.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.exam.pojo.EnterpriseOrders;

public interface EnterpriseOrdersService extends IService<EnterpriseOrders> {
    //查看历史订单
    Page selectEnterOrder(Page<EnterpriseOrders> page, QueryWrapper<EnterpriseOrders> qw);

}
