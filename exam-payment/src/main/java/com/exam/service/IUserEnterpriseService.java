package com.exam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.exam.pojo.UserEnterprise;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 霸霸
 * @since 2020-12-17
 */
public interface IUserEnterpriseService extends IService<UserEnterprise> {

}
