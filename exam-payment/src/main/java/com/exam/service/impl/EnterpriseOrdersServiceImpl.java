package com.exam.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.exam.mapper.EnterpriseOrdersMapper;
import com.exam.pojo.EnterpriseOrders;
import com.exam.service.EnterpriseOrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EnterpriseOrdersServiceImpl extends ServiceImpl<EnterpriseOrdersMapper, EnterpriseOrders> implements EnterpriseOrdersService {

    @Autowired
    private EnterpriseOrdersMapper enterpriseOrdersMapper;

    //查看历史订单
    @Override
    public Page selectEnterOrder(Page<EnterpriseOrders> page, QueryWrapper<EnterpriseOrders> qw) {
        return enterpriseOrdersMapper.selectEnterOrder(page,qw);
    }
}
