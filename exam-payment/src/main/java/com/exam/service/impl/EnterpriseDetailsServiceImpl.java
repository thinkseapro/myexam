package com.exam.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.exam.mapper.EnterpriseDetailsMapper;
import com.exam.pojo.EnterpriseDetails;
import com.exam.service.EnterpriseDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 企业试卷详情
 */
@Service
public class EnterpriseDetailsServiceImpl extends ServiceImpl<EnterpriseDetailsMapper, EnterpriseDetails> implements EnterpriseDetailsService {
    @Autowired
    private EnterpriseDetailsMapper enterpriseDetailsMapper;

    /*查询企业试卷*/
    @Override
    public Page<EnterpriseDetails> enterpriseList(Page<EnterpriseDetails> page, QueryWrapper<EnterpriseDetails> qw) {
        return enterpriseDetailsMapper.enterprisePage(page,qw);
    }
    //显示首页企业试卷列表
    @Override
    public Page<EnterpriseDetails> indexenterprise(Page<EnterpriseDetails> page, QueryWrapper<EnterpriseDetails> qw) {
        return enterpriseDetailsMapper.indexenterprise(page,qw);
    }
}
