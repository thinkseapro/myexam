package com.exam;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 企业试卷详情
 */
@SpringBootApplication
@MapperScan("com.exam.mapper")
@EnableDiscoveryClient

public class EnterPriseApplication {
    public static void main(String[] args) {
        SpringApplication.run(EnterPriseApplication.class,args);
    }
}
