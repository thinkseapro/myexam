package com.exam.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.exam.pojo.UserEnterprise;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 霸霸
 * @since 2020-12-17
 */
@Mapper
public interface UserEnterpriseMapper extends BaseMapper<UserEnterprise> {

}
