package com.exam.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.pojo.EnterpriseDetails;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 企业试卷详情
 *
 */
@Mapper
public interface EnterpriseDetailsMapper extends BaseMapper<EnterpriseDetails> {
    //查询企业试卷
    Page<EnterpriseDetails> enterprisePage(Page<EnterpriseDetails> page, @Param(Constants.WRAPPER) QueryWrapper<EnterpriseDetails> qw);


    Page<EnterpriseDetails> indexenterprise(Page<EnterpriseDetails> page, @Param(Constants.WRAPPER) QueryWrapper<EnterpriseDetails> qw);

}
