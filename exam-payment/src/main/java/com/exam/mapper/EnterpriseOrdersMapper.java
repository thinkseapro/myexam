package com.exam.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.pojo.EnterpriseOrders;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface EnterpriseOrdersMapper extends BaseMapper<EnterpriseOrders> {
    //查看历史订单
    Page selectEnterOrder(Page<EnterpriseOrders> page, @Param(Constants.WRAPPER) QueryWrapper<EnterpriseOrders> qw);
}
