package com.exam.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 题目+试卷中间表
 * </p>
 *
 * @author author
 * @since 2020-12-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("exam_topic_examination")
@ApiModel(value="ExamTopicExamination对象", description="题目+试卷中间表")
public class ExamTopicExamination implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(value = "te_id", type = IdType.AUTO)
    private Integer teId;

    @ApiModelProperty(value = "题目主键")
    private Integer topicId;

    @ApiModelProperty(value = "试卷主键")
    private String examinationId;


}
