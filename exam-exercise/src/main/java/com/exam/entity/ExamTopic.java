package com.exam.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * <p>
 * 题目表
 * </p>
 *
 * @author author
 * @since 2020-12-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("exam_topic")
@ApiModel(value="ExamTopic对象", description="题目表")
@ToString
public class ExamTopic implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "题目主键")
    @TableId(value = "topic_id", type = IdType.AUTO)
    private Integer topicId;

    @ApiModelProperty(value = "创建者id")
    private String userId;

    @ApiModelProperty(value = "题目名称")
    private String topicTitle;

    @ApiModelProperty(value = "难易度id")
    private Integer levelId;

    @ApiModelProperty(value = "类型id")
    private Integer typeId;

    @ApiModelProperty(value = "科目id")
    private Integer subjectId;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "0:练习题目 1:考试题目 2: 企业题目")
    private Integer topicFlag;

    @ApiModelProperty(value = "该题分数")
    private String topicScore;

    @ApiModelProperty(value = "答案解析")
    private String parse;

    @ApiModelProperty(value = "描述")
    private String common;

    @ApiModelProperty(value = "是否删除delete")
    private Integer isDelete;

    @ApiModelProperty(value = "题目 所属年级")
    private Integer gradeId;

    @ApiModelProperty(value = "正确答案")
    private Integer topicResult;


}
