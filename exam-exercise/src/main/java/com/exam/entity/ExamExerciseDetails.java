package com.exam.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 考试|练习 详情表
 * </p>
 *
 * @author author
 * @since 2020-12-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("exam_exercise_details")
@ApiModel(value="ExamExerciseDetails对象", description="考试|练习 详情表")
public class ExamExerciseDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "考试|练习 详情id")
    @TableId(value = "exam_exercise_details_id", type = IdType.AUTO)
    private Integer examExerciseDetailsId;

    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "用时时长")
    private LocalTime duration;

    @ApiModelProperty(value = "状态--0:练习中 1:已提交")
    private Integer detailsState;

    @ApiModelProperty(value = "分数")
    private String detailsScore;

    @ApiModelProperty(value = "提交时间")
    private LocalDateTime endTime;

    @ApiModelProperty(value = "开始时间")
    private LocalDateTime startTime;

    @ApiModelProperty(value = "科目id")
    private Integer subjectId;

    @ApiModelProperty(value = "0:练习详情,1考试详情")
    private Integer flag;

    @ApiModelProperty(value = "<练习:为空,考试:试卷主键>")
    private Integer examinationId;

    @ApiModelProperty(value = "作弊(是否异常<练习:为空,考试:0异常||1正常>)")
    private Integer isException;

    @ApiModelProperty(value = "是否删除0未删||1已删")
    private Integer isDelete;


}
