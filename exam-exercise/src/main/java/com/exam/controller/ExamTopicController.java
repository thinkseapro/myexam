package com.exam.controller;


import com.exam.entity.ExamExerciseDetails;
import com.exam.pojo.Subject;
import com.exam.service.IExamExerciseDetailsService;
import com.exam.service.IExamExerciseTopicService;
import com.exam.service.IExamTopicService;
import com.exam.util.ScoreUtil;
import com.exam.util.TopicRecordCast;
import com.exam.vo.ExamExerciseTopicVo;
import com.exam.vo.ExaminationExamVo;
import com.exam.vo.TopicVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 题目表 前端控制器
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
@RestController
@RequestMapping("/exam")
public class ExamTopicController {

    /**
     * 题目业务逻辑实例
     */
    @Autowired
    private IExamTopicService ites;
    /**
     * 答题详情业务逻辑实例
     */
    @Autowired
    private IExamExerciseDetailsService ieeds;
    /**
     * 答题记录业务逻辑实例
     */
    @Autowired
    private IExamExerciseTopicService ieets;

    /**
     * 分数及答题记录帮助类
     */
    @Autowired
    private ScoreUtil su;
    /**
     * 题目&答题记录互转帮助类
     */
    @Autowired
    private TopicRecordCast trc;

    /**
     * 随机试题生成类
     * 开始即生成答题记录
     * 退出不提交还可进入
     *
     * @param userId 用户编号
     * @param subId  科目编号
     * @return 类似试卷的答题集合
     */
    @RequestMapping("/randomTopic")
    public ExaminationExamVo randomTopic(@RequestParam(value = "userId", required = false) String userId, @RequestParam(value = "subId", required = false) Integer subId) {
        //设置用户练习未完成详情编号
        Integer i = ieeds.selectUnfinishedExercise(userId, subId);
        //随机试题
        ExaminationExamVo examinationExamVo = new ExaminationExamVo();
        //判断是否存在练习未完成
        if (i != null) {
            List<ExamExerciseTopicVo> elist = ieets.selUnfinishedTopic(userId, subId);
            examinationExamVo = trc.changType(elist);
            //设置题目详情编号
            examinationExamVo.setExamExerciseDetailsId(i);
            //放入题目数
            examinationExamVo.setTopicSum(elist.size());
            //定义名称
            examinationExamVo.setExamName("随机练习");
            return examinationExamVo;
        }
        //实例化详情
        ExamExerciseDetails eed = new ExamExerciseDetails();
        eed.setFlag(0);
        eed.setSubjectId(subId);
        eed.setUserId(userId);
        eed.setDetailsState(2);
        eed.setStartTime(LocalDateTime.now());
        eed.setIsDelete(0);
        //新增练习详情
        ieeds.save(eed);

        //获取新增详情编号
        int did = eed.getExamExerciseDetailsId();

        //获取用户年级编号
        Integer gradeId = ites.selGrade(userId);

        //获取随机题目集合
        List<TopicVo> randomList = ites.randomTopic(gradeId, subId);

        //循环遍历随机题目集合并生成答题记录
        randomList.forEach(e -> {
            //实例化答题记录
            ExamExerciseTopicVo eetv = new ExamExerciseTopicVo();
            //设置答题记录用户编号
            eetv.setUserId(eed.getUserId());
            //设置答题记录详情编号
            eetv.setExamExerciseDetailsId(did);
            //转换题目实体为答题记录实体
            BeanUtils.copyProperties(e, eetv);
            //生成新的答题记录
            su.examScore(eetv);
        });

        List<ExamExerciseTopicVo> elist = ieets.selUnfinishedTopic(userId, subId);
        examinationExamVo = trc.changType(elist);
        //设置题目详情编号
        examinationExamVo.setExamExerciseDetailsId(did);
        //放入题目数
        examinationExamVo.setTopicSum(elist.size());
        //定义名称
        examinationExamVo.setExamName("随机练习");
        return examinationExamVo;
    }


    @RequestMapping("/mistakeTopic")
    public List<TopicVo> mistakeTopic() {
        return ites.selMistake();
    }


}
