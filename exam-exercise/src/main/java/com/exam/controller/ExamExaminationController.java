package com.exam.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.exam.entity.ExamExamination;
import com.exam.pojo.Examination;
import com.exam.service.IExamExaminationService;
import com.exam.service.IExamExerciseDetailsService;
import com.exam.service.IExamExerciseTopicService;
import com.exam.service.IExaminationService;
import com.exam.vo.ExaminationExamVo;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 * 试卷表 前端控制器
 * </p>
 *
 * @author author loop
 * @since 2020-12-10
 */
@RestController
@RequestMapping("/exam")
@CrossOrigin(maxAge = 3000)
public class ExamExaminationController {
    @Autowired
    private IExaminationService iExaminationService;

    /**
     * 试卷服务层实例
     */
    @Autowired
    private IExamExaminationService iees;

    /**
     * 考试详情业务逻辑实例
     */
    @Autowired
    private IExamExerciseDetailsService ieeds;
    /**
     * 答题记录业务逻辑实例
     */
    @Autowired
    private IExamExerciseTopicService ieets;

    /**
     * 开始考试
     *
     * @return 返回试卷试题详情
     */
    @RequestMapping("/startExam")
    @ResponseBody
    public ExaminationExamVo startExam(@RequestParam  String uid,@RequestParam(value = "eeid", required = false)  Integer eeid) {
        ExaminationExamVo e = iees.selExamination(uid,eeid);
        return e;
    }

    /**
     * 预加载试卷
     *
     * @return 返回当天需要考试的试卷
     */
    @RequestMapping("/readyExam")
    @ResponseBody
    @CrossOrigin
    public List<ExaminationExamVo> readyExam() {
        return iees.readyExamination();
    }

    /**
     * 查看考试详情
     *
     * @return
     */
    @RequestMapping("/examDeatils")
    @ResponseBody
    @CrossOrigin
    public ExaminationExamVo examDeatils(@RequestParam  String userId , @RequestParam  String deatilsId) {
        return iees.selExamDeatils(userId,deatilsId);
    }

    /**
     * 查看练习详情
     *
     * @return
     */
    @RequestMapping("/exerciseDeatils")
    @ResponseBody
    @CrossOrigin
    public ExaminationExamVo exerciseDeatils(@RequestParam  String userId , @RequestParam  String deatilsId) {
        return iees.selExerciseDeatils(userId,deatilsId);
    }
    //根据试卷id查询试卷
    @RequestMapping("/getOneByEid")
    @ResponseBody
    @CrossOrigin
    public Examination getOneByEid(@RequestParam int eid){
        return  iExaminationService.getOne(new QueryWrapper<Examination>().eq("examination_id",eid));
    }
}
