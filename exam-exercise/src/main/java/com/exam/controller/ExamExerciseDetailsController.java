package com.exam.controller;


import com.exam.service.IExamExaminationService;

import com.exam.service.IExamExerciseTopicService;
import com.exam.util.ScoreUtil;
import com.exam.util.TopicRecordCast;
import com.exam.vo.ExamExerciseTopicVo;
import com.exam.vo.ExaminationExamVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 * 考试|练习 详情表 前端控制器
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
@RestController
@RequestMapping("/exam")
public class ExamExerciseDetailsController {


    /**
     * 试卷业务逻辑实例
     */
    @Autowired
    private IExamExaminationService iees;

    /**
     * 检测答题工具类
     */
    @Autowired
    private ScoreUtil su;

    /**
     * 答题记录
     */
    @Autowired
    private IExamExerciseTopicService iets;
    /**
     * 题目&答题记录转换
     */
    @Autowired
    private TopicRecordCast trc;

    /**
     * 提交试卷
     * 修改考试记录
     *
     * @return
     */
    @RequestMapping("/commitExam")
    @ResponseBody
    @CrossOrigin
    public boolean commitExam(@RequestBody ExaminationExamVo obj) {

        List<ExamExerciseTopicVo> eList = trc.changType(obj);
        //考试详情编号
        Integer examDid = obj.getExamExerciseDetailsId();
        //试卷编号
        Integer examId = obj.getExaminationId();
        //试卷添加成功标志
        boolean flag = false;
        try {
            flag = iees.overExam(examDid, examId, eList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return flag;
    }

    /**
     * 提交练习
     *
     * @param obj
     * @return
     */
    @RequestMapping("/commitExercise")
    @ResponseBody
    @CrossOrigin
    public boolean commitExercise(@RequestBody ExaminationExamVo obj) {

        //转换试题转换为答题记录
        List<ExamExerciseTopicVo> eList = trc.changType(obj);

        //完成练习
        boolean flag = iees.overExercise(obj.getExamExerciseDetailsId(),eList);

        return flag;
    }



}
