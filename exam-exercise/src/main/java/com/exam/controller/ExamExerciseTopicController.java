package com.exam.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.service.IExamExerciseTopicService;
import com.exam.vo.ExamExerciseTopicErrorVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
@RestController
@RequestMapping("/exam-exercise-topic")
public class ExamExerciseTopicController {

    @Autowired
    private IExamExerciseTopicService topicService;

    @RequestMapping("/getExerciseTopicError")
    public Page<ExamExerciseTopicErrorVo> getExerciseTopicError(@RequestBody ExamExerciseTopicErrorVo errorVo){
        System.out.println(errorVo.toString());
        Page<ExamExerciseTopicErrorVo> list=topicService.getExerciseTopicError(errorVo,new Page<>(errorVo.getStartpage(),errorVo.getPagesize()));
        return list;
    }
}
