package com.exam.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 题目+试卷中间表 前端控制器
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
@RestController
@RequestMapping("/exam-topic-examination")
public class ExamTopicExaminationController {

}
