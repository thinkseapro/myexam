package com.exam.controller;


import com.exam.constant.ExamCode;
import com.exam.constant.Result;
import com.exam.pojo.ExamTopicTick;
import com.exam.service.IExamTopicTickService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 错题反馈表 前端控制器
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
@RestController
@RequestMapping("/exam-topic-tick")
public class ExamTopicTickController {

    @Autowired
    private IExamTopicTickService topicTickService;

    @RequestMapping("/addTopicTick")
    public Result addTopicTick(@RequestBody ExamTopicTick examTopicTick){
        Boolean pd=topicTickService.save(examTopicTick);
        if (pd) return Result.result(ExamCode.Exam_SUCCESS);

        return Result.result(ExamCode.Exam_FAIL);
    }


}
