package com.exam.util;

import com.exam.mapper.ExamExerciseTopicMapper;
import com.exam.mapper.ExamTopicMapper;
import com.exam.vo.ExamExerciseTopicVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 计算分数帮助类
 * @author loop
 */
@Component
public class ScoreUtil {

    /**
     * 字符转换工具类
     */
    @Autowired
    private StringSetCastUtil stringSetCastUtil;

    /**
     * 题目表数据访问层实例
     */
    @Autowired
    private ExamTopicMapper etm;

    /**
     * 答题记录表数据访问层实例
     */
    @Autowired
    private ExamExerciseTopicMapper eetm;

    /**
     * 重载类分数
     * @param eetv 答题记录集合
     * @return
     */
    public Integer examScore(List<ExamExerciseTopicVo> eetv){
        AtomicReference<Integer> score= new AtomicReference<>(0);
        eetv.forEach(e->{
            score.updateAndGet(v -> v + examScore(e));
        });
        return score.get();
    }
    /**
     * <p>计算考试成绩&新增答题记录</p>
     * @param eetv 用户做答答案集合
     */
    public Integer examScore(ExamExerciseTopicVo eetv) {

        System.out.println(eetv.getExamExerciseTopicId());
        //获取题目答案及分数集合
        Map<String, String> rsMap = etm.selScoreAndResult(eetv.getTopicId());

        //获取题目正确答案
        String realAnswer = rsMap.get("topic_result");
        //用户选择选项
        Set<String> userAnswer= eetv.getAnswerSet();
        //默认设置答题正确
        eetv.setIsture(1);


        //正确答案
        eetv.setCorrectAnswer(realAnswer);

        //判断答案是否正确
        boolean resultFlag= checkOption(realAnswer,userAnswer);
        if (resultFlag){

            //判断答题记录是否存在
         if (eetv.getExamExerciseTopicId()!=null){
             eetm.updateById(eetv);
         }else{
             eetm.insert(eetv);
         }

            return Integer.parseInt(rsMap.get("topic_score"));
        }else {
            //设置对象答题错误
            eetv.setIsture(0);
            //判断答题记录是否存在
            if (eetv.getExamExerciseTopicId()!=null){
                eetm.updateById(eetv);
            }else{
                eetm.insert(eetv);
            }
            return 0;
        }


    }


    /**
     * 检查答题是否正确
     * @param realAnswer
     * @param userAnswer
     * @return
     */
    public boolean checkOption(String realAnswer, Set<String> userAnswer){

        //用户未作答
        if (userAnswer==null){
            return false;
        }
        //用户未完全作答
        if (realAnswer.length()>userAnswer.size()){
            return false;
        }
        //将正确转换为Set
        Set<String> answer=stringSetCastUtil.stringToSet(realAnswer);

        return answer.containsAll(userAnswer);

    }
}
