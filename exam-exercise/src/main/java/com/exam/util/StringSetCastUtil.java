package com.exam.util;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * string&Set互相转换帮助类
 *
 * @author loop
 */
@Component
public class StringSetCastUtil {

    /**
     * string转换Set方法
     *
     * @param str 需要转换的字符串
     * @return 转换之后的set集合
     */
    public  Set<String> stringToSet(String str) {
        //空值返回null
        if (str==""||str==null||"null".equals(str)){
            return new HashSet<>();
        }
        System.out.println("str:"+str);
        //字符串转大写及去空格
        str=str.toUpperCase().trim();
        //初始换数string数组长度
        String[] strArray = new String[str.length()];
        //获取字符串转换的char数组
        char[] cArray = str.toCharArray();
        //循环遍历char数组
        for (int i = 0; i < cArray.length; i++) {
            //转换字符放入字符串数组
            strArray[i] = String.valueOf(cArray[i]);
        }
        //将string数组转换成List放入Set
        Set staffsSet = new HashSet<String>(Arrays.asList(strArray));
        System.out.println("staffsSet:"+staffsSet);
        return staffsSet;
    }

    /**
     * Set转换string方法
     *
     * @param strSet 需要转换的set集合
     * @return 转换之后的字符串
     */
    public  String setToString(Set<String> strSet) {
        if (strSet==null){
            return "";
        }
        //初始化空字符串
        StringBuilder str=new StringBuilder();
        //循环遍历Set
        strSet.forEach(e->{
            str.append(e);
        });
        return str.toString();
    }
}
