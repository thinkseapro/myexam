package com.exam.util;

import com.exam.service.IExamExaminationService;
import com.exam.vo.ExaminationExamVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 调用预加载试卷工具线程类
 * @author loop
 */
@Component
public class TimerUtil   {

    /**
     * 考试业务逻辑层实例
     */
    @Autowired
    private IExamExaminationService iees;

    /**
     * redis缓存工具类
     */
    @Autowired
    private RedisUtil ru;

    /**
     * 每天晚上8点执行一次
     * 每天定时安排任务进行执行
     */
    @Bean
    public  void executeEightAtNightPerDay() {
        //线程池
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        long oneDay = 24 * 60 * 60 * 1000;
        long initDelay  = getTimeMillis("16:10:40") - System.currentTimeMillis();
        initDelay = initDelay > 0 ? initDelay : oneDay + initDelay;

        executor.scheduleAtFixedRate(
                //lamada实现线程类
                ()->{
                    List<ExaminationExamVo> eList = iees.readyExamination();
                    for (ExaminationExamVo eVo : eList) {
                        ru.set("Examination"+eVo.getExaminationId(),eVo);
                    }
                },
                initDelay,
                oneDay,
                TimeUnit.MILLISECONDS);
    }
    /**
     * 获取指定时间对应的毫秒数
     * @param time "HH:mm:ss"
     * @return
     */
    private  long getTimeMillis(String time) {
        try {
            DateFormat dateFormat = new SimpleDateFormat("yy-MM-dd HH:mm:ss");
            DateFormat dayFormat = new SimpleDateFormat("yy-MM-dd");
            Date curDate = dateFormat.parse(dayFormat.format(new Date()) + " " + time);
            return curDate.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}

