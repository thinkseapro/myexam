package com.exam.util;

import com.exam.vo.ExamExerciseTopicVo;
import com.exam.vo.ExaminationExamVo;
import com.exam.vo.OptionVo;
import com.exam.vo.TopicVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * 题目&答题记录互转类
 */
@Component
public class TopicRecordCast {

    /**
     * 字符串&set转换工具类
     */
    @Autowired
    private StringSetCastUtil sscu;

    /**
     * 题目转换答题纪录方法
     *
     * @param evo 试卷
     * @return 整套答题记录
     */

    public List<ExamExerciseTopicVo> changType(ExaminationExamVo evo) {


        //答题记录集合
        List<ExamExerciseTopicVo> eList = new ArrayList<>();

        //循环遍历试卷
        evo.getTList().forEach(e -> {

            ExamExerciseTopicVo evo1 = new ExamExerciseTopicVo();
            if (e.getExerciseId() != null) {
                //设置答题记录编号
                evo1.setExamExerciseTopicId(e.getExerciseId());
            }
            String respond = "";
            //多选题获取各个选项是否选中,单选题获取单题答案
            if (e.getTypeId() == 1) {
                if (e.getRespond() != null && !"null".equals(e.getRespond())) {
                    respond += e.getRespond();
                }
            } else {
                //循环遍历选项答案
                for (OptionVo optionVo : e.getOList()) {
                    if (optionVo.getRespond() != null && !"null".equals(optionVo.getRespond())) {
                        respond += optionVo.getRespond();
                    }
                }
            }
            Set<String> resSet = sscu.stringToSet(respond);

            //设置答题集合
            evo1.setExerciseAnswer(respond);
            evo1.setAnswerSet(resSet);
            //设置答题题目编号
            evo1.setTopicId(e.getTopicId());
            //设置用户编号
            evo1.setUserId(evo.getUserId());
            //设置详情编号
            evo1.setExamExerciseDetailsId(evo.getExamExerciseDetailsId());
            //添加进入答题集合
            eList.add(evo1);
        });

        return eList;
    }

    /**
     * 答题记录转换题目 方法
     *
     * @param eList 答题记录集合
     */
    public ExaminationExamVo changType(List<ExamExerciseTopicVo> eList) {
        ExaminationExamVo examinationExamVo = new ExaminationExamVo();

        List<TopicVo> topicVoList = new ArrayList<>();
        eList.forEach(e -> {

            //获取用户作答答案
            String exerciseAnswer = e.getExerciseAnswer();
            TopicVo tv = e.getTopic();
            //设置答题记录编号
            tv.setExerciseId(e.getExamExerciseTopicId());
            //判断用户是否作答
            if (exerciseAnswer != null) {
                //如果答题记录为单选 直接设置用户作答答案 如果为多选 循环设置每选项答案
                if (tv.getTypeId() == 1) {
                    //设置用户作答答案
                    tv.setRespond(exerciseAnswer);
                } else {
                    //循环设置用户作答答案
                    tv.getOList().forEach(o -> {
                        //获取题目选项标识
                        String OptionFlag = o.getOptionFlag();
                        //判断用户是否选择此选项
                        if (OptionFlag.contains(exerciseAnswer)) {
                            //设置用户作答答案
                            o.setRespond(OptionFlag);
                        }
                    });
                }
            }
            //加入题目集合
            topicVoList.add(tv);
        });
        examinationExamVo.setTList(topicVoList);
        return examinationExamVo;
    }
}
