package com.exam.mapper;

import com.exam.entity.ExamTopic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.exam.pojo.Subject;
import com.exam.vo.TopicVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


/**
 * <p>
 * 题目表 Mapper 接口
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
public interface ExamTopicMapper extends BaseMapper<ExamTopic> {

    /**
     * 根据科目随机刷题
     * @return 随机二十道题目
     */
    List<TopicVo> randomTopic(@Param("gradeId")Integer gradeId,@Param("subId") Integer subId);

    /**
     * 查询错题
     * @return 用户所有错题
     */
    List<TopicVo> selMistake();

    /**
     * 查询题目分数及答案
     * @return 成绩及答案
     * @param topicId 题目编号
     */
    Map<String,String> selScoreAndResult(Integer topicId);

    /**
     * 查询用户年级编号
     * @param userId 用户编号
     * @return
     */
    Integer selGrade(@Param("userId") String userId);


}
