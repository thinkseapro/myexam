package com.exam.mapper;

import com.exam.pojo.ExamTopicTick;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 错题反馈表 Mapper 接口
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
public interface ExamTopicTickMapper extends BaseMapper<ExamTopicTick> {

}
