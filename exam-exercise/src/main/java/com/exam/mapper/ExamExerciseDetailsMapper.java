package com.exam.mapper;

import com.exam.entity.ExamExerciseDetails;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 考试|练习 详情表 Mapper 接口
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
public interface ExamExerciseDetailsMapper extends BaseMapper<ExamExerciseDetails> {

    /**
     * 获取用户考试时长
     * @param eedId 考试详情编号
     * @return
     */
    String getDuration(Integer eedId);

    /**
     *获取用户异常状态
     * @param eedId 考试详情编号
     * @return 用户考试异常状态 作弊(是否异常<练习:为空,考试:0异常||1正常>)
     */
    Integer selExamExeState(Integer eedId);

    /**
     * 计算用户考试用时时长
     * @param eedId
     * @param endTime 用户提交时间
     * @return
     */
   String mathCommitTime(@Param("endTime")LocalDateTime endTime,@Param("eedId") Integer eedId);



    /**
     * 查询考试是否有未进行完的练习
     * @param userId
     * @param subId
     * @return 未完成练习数
     */
    Integer selectUnfinishedExercise(@Param("userId") String userId,@Param("subId") Integer subId);

    /**
     *
     * @param detailsId 练习详情编号
     * @return 用户练习时长及分数
     */
    ExamExerciseDetails selTimeScore(@Param("detailsId") String detailsId);
    /**
     * 查询科目名称
     * @param subId 科目编号
     * @return
     */
    String selSubject(@Param("subId") Integer subId);
}
