package com.exam.mapper;

import com.exam.entity.ExamTopicExamination;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 题目+试卷中间表 Mapper 接口
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
public interface ExamTopicExaminationMapper extends BaseMapper<ExamTopicExamination> {

}
