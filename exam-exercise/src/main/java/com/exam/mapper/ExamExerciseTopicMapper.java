package com.exam.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.entity.ExamExerciseTopic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.exam.vo.ExamExerciseTopicErrorVo;
import com.exam.vo.ExamExerciseTopicVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
public interface ExamExerciseTopicMapper extends BaseMapper<ExamExerciseTopicVo> {
  /**
   * 获取用户未完成练习题
   * @param userId 用户编号
   * @param subId
   * @return 未完成练习题
   */
  List<ExamExerciseTopicVo> selUnfinishedTopic(@Param("userId") String userId,@Param("subId") Integer subId);

  Page<ExamExerciseTopicErrorVo> getExerciseTopicError(@Param("errorVo") ExamExerciseTopicErrorVo errorVo, Page page);

}
