package com.exam.mapper;

import com.exam.entity.ExamOptionData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.scheduling.support.SimpleTriggerContext;

/**
 * <p>
 * 选项内容表 Mapper 接口
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
public interface ExamOptionDataMapper extends BaseMapper<ExamOptionData> {

    /**
     * 查询选项所对应答案
     * @return “A,B,C...题目答案
     * @param optionId 选项编号
     */
   String selOption(Integer optionId);
}
