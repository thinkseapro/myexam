package com.exam.mapper;

import com.exam.entity.ExamExamination;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.exam.vo.ExamExerciseTopicVo;
import com.exam.vo.ExaminationExamVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 试卷表 Mapper 接口
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
@SuppressWarnings("ALL")
@Repository
public interface ExamExaminationMapper extends BaseMapper<ExamExamination> {
    /**
     * 获取考试试卷数据层接口
     * @return 试卷全部内容
     */
    ExaminationExamVo selExamination(@Param("uid") String uid, @Param("eeid") Integer eeid);

    /**
     * 查询考试时长
     * @param eId
     * @return
     */
    String  selDuration(Integer eId);

    /**
     * 准备24内将进行的考试试卷
     * @return
     */
    List<ExaminationExamVo> readyExamination();

    /**
     *查询用户考试详情记录
     * @param userId 用户编号
     * @param deatilsId 考试详情编号
     * @return 用户考试记录
     */
    ExaminationExamVo selExamDeatils(@Param("userId") String userId, @Param("detailsId") String deatilsId);

    /**
     * 查询用户练习详情
     * @param userId
     * @param deatilsId
     * @return
     */
    List<ExamExerciseTopicVo>  selExerciseDeatils(@Param("userId") String userId, @Param("detailsId") String deatilsId);


    /**
     * 查询用户是否已经提交
     * @param detailsId 详情编号
     * @return
     */
    Integer selIfCommit(@Param("detailsId") Integer detailsId);


}
