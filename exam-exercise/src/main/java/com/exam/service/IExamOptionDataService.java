package com.exam.service;

import com.exam.entity.ExamOptionData;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 选项内容表 服务类
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
public interface IExamOptionDataService extends IService<ExamOptionData> {

}
