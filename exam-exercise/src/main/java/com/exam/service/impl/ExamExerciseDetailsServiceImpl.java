package com.exam.service.impl;

import com.exam.entity.ExamExerciseDetails;
import com.exam.mapper.ExamExerciseDetailsMapper;
import com.exam.service.IExamExerciseDetailsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 考试|练习 详情表 服务实现类
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
@Service
public class ExamExerciseDetailsServiceImpl extends ServiceImpl<ExamExerciseDetailsMapper, ExamExerciseDetails> implements IExamExerciseDetailsService {

    /**
     * 练习/考试详情数据访问层实例
     */
    @Autowired
    private ExamExerciseDetailsMapper eedm;


    /**
     * 查询考试是否有未进行完的练习
     * @param userId
     * @return 未完成练习数
     */
    @Override
    public Integer selectUnfinishedExercise(String userId,Integer subId){
        return  eedm.selectUnfinishedExercise(userId,subId);
    }

}
