package com.exam.service.impl;

import com.exam.entity.ExamExamination;
import com.exam.entity.ExamExerciseDetails;
import com.exam.mapper.*;
import com.exam.service.IExamExaminationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.exam.util.ScoreUtil;
import com.exam.vo.ExamExerciseTopicVo;
import com.exam.vo.ExaminationExamVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;


/**
 * <p>
 * 试卷表 服务实现类
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
@Service
public class ExamExaminationServiceImpl extends ServiceImpl<ExamExaminationMapper, ExamExamination> implements IExamExaminationService {

    /**
     * 试卷数据访问层实例
     */
    @Autowired
    private ExamExaminationMapper eem;


    /**
     * 选项表数据访问层实例
     */
    @Autowired
    private ExamOptionDataMapper eodm;

    /**
     * 考试详情表数据访问层实例
     */
    @Autowired
    private ExamExerciseDetailsMapper eedm;

    /**
     * 计算分数，增加答题记录工具类
     */
    @Autowired
    private ScoreUtil scoreUtil;



    /**
     * 查询试卷题目
     *开始考试
     * @return 试卷题目
     */
    @Override
    public ExaminationExamVo selExamination( String uid, Integer eeid) {

        //修改答题详情-->考试状态：已提交
        ExamExerciseDetails eed = new ExamExerciseDetails();
        eed.setExamExerciseDetailsId(eeid);

        //修改考试状态：已提交
        eed.setDetailsState(2);
        int flag = eedm.updateById(eed);

        return eem.selExamination(uid, eeid);
    }


    /**
     * 完成考试
     *
     * @param eList 考生提交试卷内容
     * @param
     * @param
     */
    @Override
    public boolean overExam(Integer examDetailID, Integer eId, List<ExamExerciseTopicVo> eList) {

        int commitFlag=eem.selIfCommit(examDetailID);
        //用户已经提交禁止提交第二遍
        if (commitFlag==1){
            return false;
        }

        //修改答题详情-->考试状态：已提交
        ExamExerciseDetails eed = new ExamExerciseDetails();
        eed.setExamExerciseDetailsId(examDetailID);

        //修改考试状态
        eed.setDetailsState(1);

        //获取 提交时间
        LocalDateTime commitTime = LocalDateTime.now();

        //修改用户考试时长
        LocalTime duration = LocalTime.parse(eedm.mathCommitTime(commitTime, examDetailID));
        eed.setDuration(duration);

        //存取分数属性
        AtomicReference<Integer> score = new AtomicReference<>(0);



            //新增答题记录&获取答题分数
            eList.forEach(e -> {
                score.updateAndGet(v -> v + scoreUtil.examScore(e));
            });

        //获取用户考试状态
        boolean b= checkExamState(examDetailID, eId, duration);
        //判断用户答题状态是否正常-->异常零分
        if (b){
            score.set(0);
        }
        //修改用户分数
        eed.setDetailsScore(score.toString());
        //修改考试详情
        int flag = eedm.updateById(eed);
        //返回是否修改成功
        return flag > 0;
    }

    /**
     * 提交练习
     * @param examDetailID 考试详情编号
     * @param eList 答题记录集合
     * @return
     */
    @Override
    public boolean overExercise(Integer examDetailID,List<ExamExerciseTopicVo> eList) {

        //修改答题详情-->考试状态：已提交
        ExamExerciseDetails eed = new ExamExerciseDetails();
        eed.setExamExerciseDetailsId(examDetailID);

        //修改提交时间
        LocalDateTime commitTime = LocalDateTime.now();
        eed.setEndTime(commitTime);

        //修改练习状态：已提交
        eed.setDetailsState(1);

        //修改用户练习时长
        LocalTime duration = LocalTime.parse(eedm.mathCommitTime(commitTime, examDetailID));
        eed.setDuration(duration);

        //获取并修改用户练习分数
        Integer score= scoreUtil.examScore(eList);
        eed.setDetailsScore(score.toString());

        //修改练习详情
        int flag = eedm.updateById(eed);

        //返回是否修改成功
        return flag > 0;

    }

    /**
     * 准备24小时内将要进行的考试
     *
     * @return 返回考试试卷
     */
    @Override
    public List<ExaminationExamVo> readyExamination() {

        //调用数据层方法
        return eem.readyExamination();
    }

    /**
     * 查询用户练习记录详情
     * @param userId 用户编号
     * @param deatilsId 考试详情编号
     * @return 考试详情记录
     */
    @Override
    public ExaminationExamVo selExerciseDeatils(String userId, String deatilsId) {
        //答题记录集合
        List<ExamExerciseTopicVo>  recordList =  eem.selExerciseDeatils(userId,deatilsId);
        //获取分数及时间
        ExamExerciseDetails eed = eedm.selTimeScore(deatilsId);

        //获取科目名称
        String subName=eedm.selSubject(eed.getSubjectId());

        //试卷Vo
        ExaminationExamVo eev=new ExaminationExamVo();
        eev.setRecordList(recordList);
        eev.setExaminationTotalScore(eed.getDetailsScore());
        eev.setDuration(eed.getDuration());
        eev.setExamName(subName+"练习");
        return eev;
    }

    /**
     * 查询用户考试记录详情
     * @param userId 用户编号
     * @param deatilsId 考试详情编号
     * @return 考试详情记录
     */
    @Override
    public ExaminationExamVo selExamDeatils(String userId, String deatilsId) {
        return eem.selExamDeatils(userId,deatilsId);
    }


    /**
     * 检查考试状态
     *
     * @param eedmId  用户考试详情编号
     * @param eId     考试编号
     * @param stuTime 学生考试用时时长
     * @ParseException 时间转换异常
     */
    public boolean checkExamState(Integer eedmId, Integer eId, LocalTime stuTime) {
        try {
            //用户考试状态异常-->返回false
            if (eedm.selExamExeState(eedmId) != 1) {
                return false;
            }
            //获取试卷时长
            String examDuration = eem.selDuration(eId);
            //将字符串形式的时间转化为LocalTime类型的时间
            LocalTime examTime = LocalTime.parse(examDuration);
            //考试超时零分
            if (stuTime.isBefore(examTime)) {
                return true;
            }
        }catch (Exception e){
            return false;
        }
        return false;
    }


}
