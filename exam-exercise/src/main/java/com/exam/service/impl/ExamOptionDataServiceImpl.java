package com.exam.service.impl;

import com.exam.entity.ExamOptionData;
import com.exam.mapper.ExamOptionDataMapper;
import com.exam.service.IExamOptionDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 选项内容表 服务实现类
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
@Service
public class ExamOptionDataServiceImpl extends ServiceImpl<ExamOptionDataMapper, ExamOptionData> implements IExamOptionDataService {

}
