package com.exam.service.impl;

import com.exam.entity.ExamTopic;
import com.exam.mapper.ExamTopicMapper;
import com.exam.pojo.Subject;
import com.exam.service.IExamTopicService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.exam.vo.TopicVo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 题目表 服务实现类
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
@Service
public class ExamTopicServiceImpl extends ServiceImpl<ExamTopicMapper, ExamTopic> implements IExamTopicService {

    /**
     * 题目数据层实例
     */
    @Autowired
    private  ExamTopicMapper etm;

    /**
     * 随机刷题
     * @return 随机二十道题目
     */
    @Override
    public List<TopicVo> randomTopic(Integer gradeId,Integer subId){
        return etm.randomTopic(gradeId,subId);
    }

    /**
     * 查询错题
     * @return 用户所有错题
     */
    @Override
    public List<TopicVo> selMistake() {
        return etm.selMistake();
    }

    /**
     * 查询用户年级信息
     * @param userId 用户编号
     * @return
     */
    @Override
    public Integer selGrade(String userId) {
        return etm.selGrade(userId);
    }



}
