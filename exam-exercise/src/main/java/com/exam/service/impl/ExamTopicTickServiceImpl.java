package com.exam.service.impl;

import com.exam.pojo.ExamTopicTick;
import com.exam.mapper.ExamTopicTickMapper;
import com.exam.service.IExamTopicTickService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 错题反馈表 服务实现类
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
@Service
public class ExamTopicTickServiceImpl extends ServiceImpl<ExamTopicTickMapper, ExamTopicTick> implements IExamTopicTickService {

}
