package com.exam.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.entity.ExamExerciseTopic;
import com.exam.mapper.ExamExerciseTopicMapper;
import com.exam.service.IExamExerciseTopicService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.exam.vo.ExamExerciseTopicErrorVo;
import com.exam.vo.ExamExerciseTopicVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  答题记录服务实现类
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
@Service
public class ExamExerciseTopicServiceImpl extends ServiceImpl<ExamExerciseTopicMapper, ExamExerciseTopicVo> implements IExamExerciseTopicService {

    /**
     * 答题记录数据层实例
     */
    @Autowired
    private ExamExerciseTopicMapper eetm;


    @Autowired
    private ExamExerciseTopicMapper examExerciseTopicMapper;

    @Override
    public Page<ExamExerciseTopicErrorVo> getExerciseTopicError(ExamExerciseTopicErrorVo errorVo, Page<ExamExerciseTopicVo> page) {

        Page<ExamExerciseTopicErrorVo> iPage=examExerciseTopicMapper.getExerciseTopicError(errorVo,page);
        return iPage;
    }
    /**
     * @param userId 用户编号
     * @return 用户未完成练习
     */
    @Override
    public List<ExamExerciseTopicVo> selUnfinishedTopic(String userId,Integer subId) {
        return eetm.selUnfinishedTopic(userId,subId);
    }
}
