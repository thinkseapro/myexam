package com.exam.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.exam.pojo.CountExam;
import com.exam.pojo.ExerciseDetails;
import com.exam.vo.MyScoreDataVo;

/**
 * <p>
 *  练习
 * </p>
 *
 * @author loop
 * @since 2020-12-09
 */
public interface IExerciseDetailsService extends IService<ExerciseDetails> {


}
