package com.exam.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.exam.pojo.Examination;

public interface IExaminationService extends IService<Examination> {
}
