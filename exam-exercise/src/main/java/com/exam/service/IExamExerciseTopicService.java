package com.exam.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.entity.ExamExerciseTopic;
import com.baomidou.mybatisplus.extension.service.IService;
import com.exam.vo.ExamExerciseTopicErrorVo;
import com.exam.vo.ExamExerciseTopicVo;

import java.util.List;

/**
 * <p>
 * 答题记录服务类
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
public interface IExamExerciseTopicService extends IService<ExamExerciseTopicVo> {
    /**
     * 获取用户未完成练习题
     * @param userId 用户编号
     * @return 未完成练习题
     */
    List<ExamExerciseTopicVo> selUnfinishedTopic(String userId,Integer subId);

    Page<ExamExerciseTopicErrorVo> getExerciseTopicError(ExamExerciseTopicErrorVo errorVo, Page<ExamExerciseTopicVo> page);
}
