package com.exam.service;

import com.exam.entity.ExamExamination;
import com.baomidou.mybatisplus.extension.service.IService;
import com.exam.vo.ExamExerciseTopicVo;
import com.exam.vo.ExaminationExamVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;

/**
 * <p>
 * 试卷表 服务类
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
@Service
public interface IExamExaminationService extends IService<ExamExamination> {
    /**
     * 获取一套考试试卷
     * @return 试卷所有内容
     */
    ExaminationExamVo selExamination(String uid, Integer eeid);

       /**
     * 完成考试
     * @param examDetailID 考试详情编号
     * @param eId 试卷编号
     * @param eList 答题记录集合
     * @return  提交是否成功
     * @throws ParseException 时间转换异常
     */
    boolean overExam(Integer examDetailID, Integer eId , List<ExamExerciseTopicVo> eList);

    /**
     * 提交练习
     * @param examDetailID 考试详情编号
     * @param eList 答题记录集合
     * @return  提交是否成功
     * @throws ParseException 时间转换异常
     */
    boolean overExercise(Integer examDetailID,List<ExamExerciseTopicVo> eList);

    /**
     * 准备24内将进行的考试试卷
     * @return
     */
    List<ExaminationExamVo> readyExamination();
    /**
     *查询用户考试详情记录
     * @param userId 用户编号
     * @param deatilsId 考试详情编号
     * @return 用户考试记录
     */
    ExaminationExamVo selExamDeatils(String userId, String deatilsId);

    /**
     * 查询用户练习详情
     * @param userId
     * @param deatilsId
     * @return
     */
    ExaminationExamVo selExerciseDeatils( String userId,String deatilsId);

}
