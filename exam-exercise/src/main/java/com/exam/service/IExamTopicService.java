package com.exam.service;

import com.exam.entity.ExamTopic;
import com.baomidou.mybatisplus.extension.service.IService;
import com.exam.pojo.Subject;
import com.exam.vo.TopicVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * <p>
 * 题目表 服务类
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
public interface IExamTopicService extends IService<ExamTopic> {

    /**
     * 随机刷题
     * @return 随机二十道题目
     */
    List<TopicVo> randomTopic(Integer gradeId,Integer subId);
    /**
     * 查询错题
     * @return 用户所有错题
     */
    List<TopicVo> selMistake();
    /**
     * 查询用户年级编号
     * @param userId 用户编号
     * @return
     */
    Integer selGrade(String userId);

}
