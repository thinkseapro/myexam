package com.exam.service;

import com.exam.pojo.ExamTopicTick;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 错题反馈表 服务类
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
public interface IExamTopicTickService extends IService<ExamTopicTick> {

}
