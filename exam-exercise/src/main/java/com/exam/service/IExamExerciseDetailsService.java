package com.exam.service;

import com.exam.entity.ExamExerciseDetails;
import com.baomidou.mybatisplus.extension.service.IService;
import com.exam.vo.ExamExerciseDeatilsVo;

import java.util.List;

/**
 * <p>
 * 考试|练习 详情表 服务类
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
public interface IExamExerciseDetailsService extends IService<ExamExerciseDetails> {


    /**
     * 查询考试是否有未进行完的练习
     * @param userId
     * @return 未完成练习数
     */
    Integer selectUnfinishedExercise(String userId,Integer subId);
}
