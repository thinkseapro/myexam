/*---添加一个请求拦截器---*/
axios.interceptors.request.use(function (config) {
    console.log(sessionStorage.getItem("Exam-Authorization"));
    let token = sessionStorage.getItem("Exam-Authorization");
    if (token!=null) {
        config.headers.common['Exam-Authorization'] = token;
    }
    return config;
}, function (error) {
    // Do something with request error
    console.info("error: ");
    console.info(error);
    return Promise.reject(error);
});