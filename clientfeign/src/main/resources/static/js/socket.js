//websocket
function socket() {
    var websocket = null;
    if('WebSocket' in window) {
        const username=$(".username").val();
        websocket = new WebSocket('ws://localhost:10001/webSocket/'+username);
    }else {
        alert('该浏览器不支持websocket!');
    }

    websocket.onopen = function (event) {
        console.log('建立连接');
    }

    websocket.onclose = function (event) {
        console.log('连接关闭');
    }

    websocket.onmessage = function (event) {
        console.log('收到消息:' + event.data)
        //所要执行的操作
        //window.location.href("");
        alert("执行退出")
    }

    websocket.onerror = function () {
        alert('websocket通信发生错误！');
    }

    window.onbeforeunload = function () {
        websocket.close();
    }
}