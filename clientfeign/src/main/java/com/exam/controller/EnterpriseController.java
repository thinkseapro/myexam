package com.exam.controller;

import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.config.AlipayConfig;
import com.exam.constant.Result;
import com.exam.feign.EnterpriseFeign;
import com.exam.feign.ExamFeign;
import com.exam.feign.ScoreFeign;
import com.exam.feign.UserFeign;
import com.exam.pojo.*;
import com.exam.service.AlipayService;
import com.exam.util.AESUtil;
import com.exam.util.JwtUtil;
import com.exam.util.SnowflakeUtil;
import com.exam.vo.EnterpriseDetailsVo;
import com.exam.vo.EnterpriseOrdersVo;
import com.exam.vo.ExaminationVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Controller

public class EnterpriseController {

    @Autowired
    private EnterpriseFeign enterpriseFeign;
    @Autowired
    private SnowflakeUtil snowflakeUtil;
    @Autowired
    AlipayService alipayService;
    @Autowired
    private UserFeign userFeign;
    @Autowired
    private ScoreFeign scoreFeign;
    @Autowired
    private ExamFeign examFeign;


    //查看企业试卷
    @RequestMapping("/enterprise-topics")
    public  String getEnterpriselist(Model model){
        EnterpriseDetails ed=new EnterpriseDetails();
        ed.setEnterpriseDetailsType(0);
        Page<EnterpriseDetails> edPage=enterpriseFeign.getenterpriseList(ed,1,10);  //限量抢购

        EnterpriseDetails jed=new EnterpriseDetails();
        jed.setEnterpriseDetailsType(1);
        Page<EnterpriseDetails> jedPage=enterpriseFeign.getenterpriseList(jed,1,10);  //金典试题
        String token= AESUtil.encryptIntoHexString(JwtUtil.GetToken());  //获取token
        Result user=userFeign.getUser();
        model.addAttribute("user",user);
        model.addAttribute("token",token);
        model.addAttribute("edPage",edPage);
        model.addAttribute("jedPage",jedPage);
        return "enterprise-topics";
    }

    //查询点击立即抢购商品信息跳转到确认订单页面
    @RequestMapping("/down_order/{eid}")
    public String downOrder(@PathVariable int eid,Model model){
        EnterpriseDetails ed=new EnterpriseDetails();
        ed.setExaminationId(eid);
        Page<EnterpriseDetails> oneEd=enterpriseFeign.getenterpriseList(ed,1,1);
        String token= AESUtil.encryptIntoHexString(JwtUtil.GetToken());  //获取token
        Result user=userFeign.getUser();
        model.addAttribute("user",user);
        model.addAttribute("token",token);
        model.addAttribute("oneEd",oneEd);
        return "down_order";
    }

    //支付逻辑：添加订单表 调用沙箱支付 判断付款成功 修改企业试卷表已售数量 添加用户企业表 新增考试练习详情表
    //新增企业试卷订单
    @RequestMapping("/insertEnterOrder")
    @ResponseBody
    public String insertEnterpriseOrder(EnterpriseDetailsVo edv, ExaminationVo examinationVo, RedirectAttributes attr) throws AlipayApiException {
        EnterpriseOrdersVo eov=new EnterpriseOrdersVo();
        eov.setEnterpriseOrdersNumber(snowflakeUtil.snowflakeId());   //订单编号
        eov.setEnterpriseDetailsId(edv.getEnterpriseDetailsId());   //订单详情id
        eov.setEnterpriseOrdersPrice(edv.getEnterpriseDetailsDiscount());  //订单价格
        eov.setIspay(0);  //是否支付
        eov.setEnterpriseOrdersCreatetime(LocalDateTime.now());   //订单创建时间
        String userid=JwtUtil.getUserId(JwtUtil.GetToken());  //获取用户id
        eov.setUserId(userid);  //下单用户
        Examination examination=new Examination();   //考试表
        BeanUtils.copyProperties(examinationVo,examination);
        EnterpriseOrders enterpriseOrders=new EnterpriseOrders();
        enterpriseOrders.setEnterpriseDetailsId(edv.getEnterpriseDetailsId());  //详情id
        enterpriseOrders.setExamination(examination); //试卷相关信息
        BeanUtils.copyProperties(eov,enterpriseOrders);
        boolean flag=enterpriseFeign.insertEnterOrder(enterpriseOrders);
         String result= alipayService.aliPay(enterpriseOrders);
        //----------------------------------------^新增订单表结束
        attr.addFlashAttribute("eo", enterpriseOrders);
        attr.addFlashAttribute("edv",edv);
        return result;
    }
    //付款
    @RequestMapping("/pay")
    public String payMent(HttpServletRequest request,Model model) {
        try {
            /*Map<String,String[]> requestParams = request.getParameterMap();
            for(String key:requestParams.keySet()){
                String value=request.getParameter(key);
                System.out.println("参数名，"+key+"--->参数值"+value);
            }*/
                //交易状态
                String trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"),"UTF-8");
                String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"),"UTF-8");
                if(trade_status.equals("TRADE_SUCCESS")){
                        EnterpriseOrders oneOrder=enterpriseFeign.getOneByOrdersNumber(Long.parseLong(out_trade_no));//获取订单
                        System.out.println("EnterpriseOrders="+oneOrder);

                        boolean flag=enterpriseFeign.updateEnterOrders(oneOrder.getEnterpriseOrdersNumber());
                        //--------------------------------------^修改订单状态
                        EnterpriseDetails ed=enterpriseFeign.getOneByDetailsId(oneOrder.getEnterpriseDetailsId()); //获取企业试卷详情
                        EnterpriseDetails enterpriseDetails=new EnterpriseDetails();
                        enterpriseDetails.setEnterpriseDetailsId(ed.getEnterpriseDetailsId());
                        enterpriseDetails.setEnterpriseDetailsBuyNum(ed.getEnterpriseDetailsBuyNum()+1);
                        boolean flag1=enterpriseFeign.updateEnterDetails(enterpriseDetails);
                        //----------------------------------------^修改企业试卷详情表的已购数量结束
                        UserEnterprise userEnterprise=new UserEnterprise();
                        userEnterprise.setUserId(oneOrder.getUserId());
                        userEnterprise.setEnterpriseDetailsId(oneOrder.getEnterpriseDetailsId());
                        boolean flag2=enterpriseFeign.insertUserEnterprise(userEnterprise);
                        //----------------------------------------^新增用户-企业试卷中间表结束
                        Examination examination=examFeign.getOneByEid(ed.getExaminationId());
                        ExerciseDetails exerciseDetails=new ExerciseDetails();
                        exerciseDetails.setUserId(oneOrder.getUserId());
                        exerciseDetails.setDetailsState(2);
                        exerciseDetails.setStartTime(LocalDateTime.now());
                        exerciseDetails.setFlag(1);
                        exerciseDetails.setSubjectId(examination.getSubjectId());
                        exerciseDetails.setExaminationId(examination.getExaminationId());
                        boolean flag3=scoreFeign.insertExerciseDetails(exerciseDetails);
                       //----------------------------------------^新增考试练习详情表
                    System.out.println(flag+"--"+flag1+"---"+flag2+"---"+flag3);
                }
                System.out.println("success");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "orderOver";
    }

    //查询企业订单
    @RequestMapping("/pay_orders")
    public String selectEnterpriseOrder(Model model){
        String token= AESUtil.encryptIntoHexString(JwtUtil.GetToken());  //获取token
        Result user=userFeign.getUser();
        model.addAttribute("user",user);
        model.addAttribute("token",token);
        return "pay_orders";
    }
    //流加载已购企业试题
    @RequestMapping("/ygqy")
    @ResponseBody
    public String ygqy(){
        Page<EnterpriseOrders> eoPage=enterpriseFeign.enterOrderPage(0);  //已购企业试题
        String js= JSON.toJSONString(eoPage);
        return js;
    }
    //流加载已购经典试题
    @RequestMapping("/ygjd")
    @ResponseBody
    public  String ygjd(){
        Page<EnterpriseOrders> jeoPage=enterpriseFeign.enterOrderPage(1);  //已购经典试题
        String js=JSON.toJSONString(jeoPage);
        return js;
    }
    @RequestMapping("/orderOver")
    public String orderOver(){

         return "orderOver";
    }


}
