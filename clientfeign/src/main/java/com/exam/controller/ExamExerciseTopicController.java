package com.exam.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import com.exam.feign.ExamExerciseTopicFeign;
import com.exam.feign.ExamExerciseTopicFeign;
import com.exam.feign.ScoreFeign;
import com.exam.pojo.Subject;
import com.exam.util.JwtUtil;
import com.exam.vo.ExamExerciseTopicErrorVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 选项内容表 前端控制器
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
@RestController
@RequestMapping("/exam-exercise-topic")
@CrossOrigin(maxAge = 3600)
public class ExamExerciseTopicController {

    @Autowired
    private ExamExerciseTopicFeign examOptionDataFeign;
    @Autowired
    private ScoreFeign scoreFeign;

    @RequestMapping("/getExerciseTopicError")
    public Page<ExamExerciseTopicErrorVo> getExerciseTopicError(@RequestBody ExamExerciseTopicErrorVo errorVo){
        String uid=JwtUtil.getUserId(JwtUtil.GetToken());
        errorVo.setUserId(uid);
        System.out.println(errorVo.toString());
        return examOptionDataFeign.getExerciseTopicError(errorVo);
    }

    @RequestMapping("/subjectList")
    public List<Subject> subjectList(){

        return scoreFeign.getSubList();
    }

}
