package com.exam.controller;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.constant.Result;
import com.exam.feign.EnterpriseFeign;
import com.exam.feign.ScoreFeign;
import com.exam.feign.UserFeign;
import com.exam.pojo.CountExam;
import com.exam.pojo.EnterpriseDetails;
import com.exam.pojo.ExerciseDetails;
import com.exam.pojo.Subject;
import com.exam.util.AESUtil;
import com.exam.util.JwtUtil;
import com.exam.vo.MyScoreDataVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
/**
 * (查看成绩中心<练习,考试>)
 * (查看考试中心）
 */
@Controller
public class ScoreController {
    @Autowired
    private ScoreFeign scoreFeign;
    @Autowired
    private UserFeign userFeign;
    @Autowired
    private EnterpriseFeign enterpriseFeign;

    //加载成绩中心科目下拉列表
    @RequestMapping("/sublist")
    public String getSubList(Model model){
        List<Subject> sublist=new ArrayList();
        model.addAttribute("sublist",sublist);
        return "myscore";
    }

    //根据科目查询考试/练习成绩
    @RequestMapping("/scoreList")
    @ResponseBody
    public MyScoreDataVo scoreList(Integer page, Integer limit, @RequestParam int flag,
                                   @RequestParam Integer subjectId,@RequestParam Integer detailsState,
                                   Model model){
        ExerciseDetails exerciseDetails=new ExerciseDetails();
        exerciseDetails.setFlag(flag);
        exerciseDetails.setSubjectId(subjectId);
        exerciseDetails.setDetailsState(detailsState);
        String userid=JwtUtil.getUserId(JwtUtil.GetToken());
        exerciseDetails.setUserId(userid);
        String token= AESUtil.encryptIntoHexString(JwtUtil.GetToken());  //获取token
        model.addAttribute("token",token);
        System.out.println("嘻嘻哈哈"+scoreFeign.getScoreDetails(page,limit,exerciseDetails));
        return scoreFeign.getScoreDetails(page,limit,exerciseDetails);
    }
    /**
     * 跳转到成绩中心并加载出科目下拉表
     * @param model
     * @return
     */
    @RequestMapping("/myscore")
    public String gotoMyScore(Model model){
        List<Subject> sublist=scoreFeign.getSubList();
        String token= AESUtil.encryptIntoHexString(JwtUtil.GetToken());  //获取token
        Result user=userFeign.getUser();
        model.addAttribute("user",user);
        model.addAttribute("token",token);
        model.addAttribute("sublist",sublist);
        return "myscore";
    }

    /**
     * 跳转到考试中心并加载出科目下拉表
     * @param model
     * @return
     */
    @RequestMapping("/examlist")
    public String gotoexamlist(Model model){
        List<Subject> sublist=scoreFeign.getSubList();
        String token= AESUtil.encryptIntoHexString(JwtUtil.GetToken()); //获取token
        String userid=JwtUtil.getUserId(JwtUtil.GetToken());
        ExerciseDetails e=new ExerciseDetails();
        e.setFlag(1);
        e.setUserId(userid);
        Result user=userFeign.getUser();
        CountExam ce=scoreFeign.getCountExam(e);
        model.addAttribute("token",token);
        model.addAttribute("sublist",sublist);
        model.addAttribute("user",user);
        model.addAttribute("ce",ce);
        return "examlist";
    }
    //根据状态，科目查询考试详情
    @RequestMapping("/getexamList")
    @ResponseBody
    public MyScoreDataVo examList(Integer page, Integer limit, @RequestParam int flag,
                                  @RequestParam Integer subjectId,@RequestParam Integer detailsState,Model model){
        String userid=JwtUtil.getUserId(JwtUtil.GetToken());
        ExerciseDetails exerciseDetails=new ExerciseDetails();
        exerciseDetails.setUserId(userid);
        exerciseDetails.setFlag(flag);
        exerciseDetails.setDetailsState(detailsState);
        exerciseDetails.setSubjectId(subjectId);
        String token= AESUtil.encryptIntoHexString(JwtUtil.GetToken());  //获取token
        model.addAttribute("token",token);
        return scoreFeign.getExamList(page,limit,exerciseDetails);
    }
    //查询首页考试列表
    @RequestMapping("/index")
    public String indexexam(Model model){
        String userid=JwtUtil.getUserId(JwtUtil.GetToken());
        Page<ExerciseDetails> index=scoreFeign.indexexam(userid);
        EnterpriseDetails enterpriseDetails=new EnterpriseDetails();
        Page<EnterpriseDetails> indexed=enterpriseFeign.indexEnterpriseDetails(enterpriseDetails);
        String token= AESUtil.encryptIntoHexString(JwtUtil.GetToken());
        /*String userid=JwtUtil.getUserId(token);
        String nickname=JwtUtil.getNickname(token);*/
        Result user=userFeign.getUser();
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        model.addAttribute("token",token);
        model.addAttribute("indexexam",index);
        model.addAttribute("indexed",indexed);
        model.addAttribute("user",user);
        return "index";
    }
    //跳转到个人中心
    @RequestMapping("/personal")
    public String x(Model model){
        String token= AESUtil.encryptIntoHexString(JwtUtil.GetToken());  //获取token
        Result user=userFeign.getUser();
        model.addAttribute("user",user);
        model.addAttribute("token",token);
        return "personal";
    }
    //跳转到自主练习
    @RequestMapping("/practiselist")
    public String practiselist(Model model){
        String token= AESUtil.encryptIntoHexString(JwtUtil.GetToken());  //获取token
        Result user=userFeign.getUser();
        model.addAttribute("user",user);
        model.addAttribute("token",token);
        return "practiselist";
    }
    //跳转到选择科目页面
    @RequestMapping("/randsubject")
    public String randsubject(Model model){
        String token= AESUtil.encryptIntoHexString(JwtUtil.GetToken());
        Result user=userFeign.getUser();
        List<Subject> sublist=scoreFeign.getSubList();
        model.addAttribute("user",user);
        model.addAttribute("token",token);
        model.addAttribute("subList",sublist);
        return "randsubject";
    }
}
