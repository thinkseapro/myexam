package com.exam.controller;

import com.exam.constant.ExamCode;
import com.exam.constant.Result;
import com.exam.feign.ExamTopicTickFeign;
import com.exam.pojo.ExamTopicTick;
import com.exam.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/exam-topic-tick")
@CrossOrigin(maxAge = 3600)
public class ExamTopicTickController {

    @Autowired
    private ExamTopicTickFeign examTopicTickFeign;

    @RequestMapping("/addTopicTick")
    public Result addTopicTick(@RequestBody ExamTopicTick examTopicTick){
        String uid= JwtUtil.getUserId(JwtUtil.GetToken());
        examTopicTick.setUserId(uid);
        return examTopicTickFeign.addTopicTick(examTopicTick);
    }

}
