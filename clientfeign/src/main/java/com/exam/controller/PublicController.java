package com.exam.controller;

import com.exam.constant.Result;
import com.exam.feign.UserFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 跳转thymeleaf公共模块
 */
@Controller
public class PublicController {

    @Autowired
    private UserFeign userFeign;

    /**
     * 写模板名可直接访问，例如localhost:9999/indexhead  可直接访问首页
     * @param url
     * @return
     */
    @RequestMapping("/{url}")
    public String redirect(@PathVariable("url") String url, Model model){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        System.out.println(request.getHeader("Exam-Authorization"));
        model.addAttribute("token",request.getHeader("Exam-Authorization"));
        return url;
    }

    /**
     * 错误页面
     * @param url
     * @param model
     * @return
     */
    @RequestMapping("/Authority/{url}")
    public String redirectError(@PathVariable("url") String url, Model model){
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        System.out.println(request.getHeader("Exam-Authorization"));
        model.addAttribute("token",request.getHeader("Exam-Authorization"));
        return "error/"+url;
    }

    @RequestMapping("/user/ver/no/mail/PasswordVerify/{tokens}")
    String mailVerify(@PathVariable String tokens, HttpServletResponse response){
        Result result=userFeign.mailVerify(tokens);
        if (result.getSuccess()){

            return "success/success";
        }
        return "失败";
    }

}
