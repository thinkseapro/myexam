package com.exam.controller;

import com.exam.constant.Result;
import com.exam.feign.ExamFeign;
import com.exam.feign.UserFeign;
import com.exam.util.AESUtil;
import com.exam.util.JwtUtil;
import com.exam.vo.ExaminationExamVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 考试练习feignController
 *
 * @author loop
 */
@Controller
public class ExamExerciseController {

    /**
     * 考试feign实例
     */
    @Autowired
    private ExamFeign examFeign;

    /**
     * 用户feign实例
     */
    @Autowired
    private UserFeign userFeign;

    /**
     *
     * @param eeid
     * @return
     */
    @CrossOrigin
    @RequestMapping("/startExam")
    @ResponseBody
    public ExaminationExamVo startExam(@RequestParam(value = "eeid", required = false) Integer eeid) {
        //获取用户编号
        String userId = JwtUtil.getUserId(JwtUtil.GetToken());
        ExaminationExamVo evo=examFeign.startExam(userId, eeid);
        return evo;
    }

    /**
     * 预加载试卷
     *
     * @return 返回当天需要考试的试卷
     */
    @RequestMapping("/readyExam")
    @ResponseBody
    public List<ExaminationExamVo> readyExam() {
        return examFeign.readyExam();
    }



    /**
     * 查看考试详情
     *
     * @return
     */
    @RequestMapping("/examDeatils")
    public String examDeatils(@RequestParam(value = "deatilsId", required = false) String deatilsId, Model model) {
        //获取token
        String token = AESUtil.encryptIntoHexString(JwtUtil.GetToken());
        //获取用户
        Result user = userFeign.getUser();
        //获取用户编号
        String userId = JwtUtil.getUserId(JwtUtil.GetToken());

        model.addAttribute("token", token);
        model.addAttribute("user", user);
        ExaminationExamVo ev = examFeign.examDeatils(userId, deatilsId);
        model.addAttribute("ExaminationExamVo", ev);
        return "eaxmdetails";
    }

    /**
     * 提交试卷
     * 修改考试记录
     *
     * @return
     */
    @RequestMapping("/commitExam")
    @ResponseBody
    @CrossOrigin
    public boolean commitExam(@RequestBody ExaminationExamVo obj) {

        obj.getTList().forEach(e->{
            System.out.println(e.getExerciseId()+":getExerciseId");
        });
        //获取用户编号
        String userId = JwtUtil.getUserId(JwtUtil.GetToken());
        obj.setUserId(userId);
        return examFeign.commitExam(obj);
    }

    /**
     * 提交练习
     *
     * @param obj
     * @return
     */
    @RequestMapping("/commitExercise")
    @ResponseBody
    @CrossOrigin
    public boolean commitExercise(@RequestBody ExaminationExamVo obj) {
        //获取用户编号
        String userId = JwtUtil.getUserId(JwtUtil.GetToken());
        obj.setUserId(userId);
        return examFeign.commitExercise(obj);
    }

    /**
     * 随机试题生成类
     * 开始即生成答题记录
     * 退出不提交还可进入
     *
     * @return 类似试卷的答题集合
     */
    @CrossOrigin
    @ResponseBody
    @RequestMapping("/randomTopic")
    public ExaminationExamVo randomTopic(@RequestParam Integer subId) {
        //获取用户编号
        String userId = JwtUtil.getUserId(JwtUtil.GetToken());
        return examFeign.randomTopic(userId, subId);
    }


    /**
     * 查看练习详情
     *
     * @return
     */
    @RequestMapping("/exerciseDeatils")
    @CrossOrigin
    public String exerciseDeatils(@RequestParam String deatilsId, Model model) {
        //获取token
        String token = AESUtil.encryptIntoHexString(JwtUtil.GetToken());
        //获取用户
        Result user = userFeign.getUser();
        //获取用户编号
        String userId = JwtUtil.getUserId(JwtUtil.GetToken());
        model.addAttribute("token", token);
        model.addAttribute("user", user);
        ExaminationExamVo ev = examFeign.exerciseDeatils(userId, deatilsId);
        model.addAttribute("ExaminationExamVo", ev);
        return "eaxmdetails";
    }

    /**
     * 跳转试题页面
     *
     * @param model
     * @return
     */
    @RequestMapping("/topic")
    public String topicX(Model model) {
        //获取token
        String token = AESUtil.encryptIntoHexString(JwtUtil.GetToken());
        Result user = userFeign.getUser();
        model.addAttribute("user", user);
        model.addAttribute("token", token);
        return "topic";
    }

    /**
     * 跳转练习页面
     *
     * @param model
     * @return
     */
    @RequestMapping("/exercise")
    public String exerciseX(Model model) {
        //获取token
        String token = AESUtil.encryptIntoHexString(JwtUtil.GetToken());
        Result user = userFeign.getUser();
        model.addAttribute("user", user);
        model.addAttribute("token", token);
        return "exercise";
    }
}
