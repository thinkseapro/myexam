package com.exam.controller;

import cn.hutool.http.HttpStatus;
import com.exam.constant.Result;
import com.exam.feign.UserFeign;
import com.exam.util.AESUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLDecoder;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserFeign userFeign;


    @RequestMapping("/ver/yz")
    public Result yz(HttpServletRequest request){
        return userFeign.yz(request);
    }

    @CrossOrigin
    @RequestMapping("/ver/no/login")
    Result login(@RequestParam String username, @RequestParam String password, HttpServletResponse response){
        Result result=userFeign.login(username,password);
        if (result.getSuccess()==true){
            String token= (String) result.getData().get("token");
            response.setHeader("Exam-Authorization", AESUtil.encryptIntoHexString(token));
            response.setHeader("Access-Control-Expose-Headers", "Exam-Authorization");
        }
        return result;
    }

    @RequestMapping("/ver/exit")
    Result exit(@RequestParam("username") String username){

        return userFeign.exit(username);
    }

    @RequestMapping("/ver/updatePassword")
    Result updatePassword(@RequestParam String password){
        return userFeign.updatePassword(password);
    }

    @RequestMapping("/ver/no/ismail")
    Result isEmail(@RequestParam String email){
        return userFeign.isEmail(email);
    }

    @RequestMapping("/ver/no/mail/resetPassword")
    Result resetPassword(@RequestParam String mail){
        return userFeign.resetPassword(mail);
    }


    @RequestMapping("/ver/no/PhoneCode")
    Result PhoneCode(@RequestParam String phone){
        return userFeign.PhoneCode(phone);
    }

    @RequestMapping("/ver/no/receiveCode")
    Result receiveCode(@RequestParam String phone,@RequestParam String code){
        return userFeign.receiveCode(phone,code);
    }

    @RequestMapping("/ver/getNickname")
    Result getTokenNickname(){
        return userFeign.getTokenNickname();
    }


    @RequestMapping("/findUserByid")
    public Result getUser(){
        return userFeign.getUser();
    }

    @RequestMapping(value = "/uploadAvatar")
    public Result uploadAvatar(@RequestParam("file") String file){

        System.out.println(file);

        return userFeign.uploadAvatar(file);
    }

    @RequestMapping(value = "/sentEmailCode")
    Result sentEmailCode(@RequestBody String email){
        String emails=null;
        try {
            emails=URLDecoder.decode(email,"UTF-8").replace("=","");
            System.out.println("email="+ emails);
        }catch (Exception e){

        }
        return userFeign.sentEmailCode(emails);
    }

    @RequestMapping(value = "/VerifyEmailCode")
    public Result VerifyEmailCode(@RequestParam String oldemail,@RequestParam String newemail,@RequestParam String code){

        return userFeign.VerifyEmailCode(oldemail,newemail,code);
    }

    @RequestMapping(value = "/sentPhoneCode")
    Result sentPhoneCode(@RequestBody String phone){
        String phones=null;
        try {
            phones=URLDecoder.decode(phone,"UTF-8").replace("=","");
            System.out.println("phone="+ phones);
        }catch (Exception e){

        }
        return userFeign.sentPhoneCode(phones);
    }

    @RequestMapping(value = "/VerifyPhoneCode")
    public Result VerifyPhoneCode(@RequestParam String oldphone,@RequestParam String newphone,@RequestParam String code){

        return userFeign.VerifyPhoneCode(oldphone,newphone,code);
    }

}
