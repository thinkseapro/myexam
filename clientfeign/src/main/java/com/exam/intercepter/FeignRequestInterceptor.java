package com.exam.intercepter;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @description拦截请求并重新设置header信息
 * @author
 * @date
 */
@Slf4j
@Configuration
public class FeignRequestInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate template) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String token = request.getHeader("Exam-Authorization");
        String userAgent = request.getHeader("User-agent");
        String contentType = request.getHeader("Content-Type");
        String forwarded=request.getHeader("x-forwarded-for");
        String IP=request.getHeader("Proxy-Client-IP");
        template.header("Content-Type", new String[]{contentType});
        template.header("User-agent", new String[]{userAgent});
        template.header("Exam-Authorization", new String[]{token});
        template.header("x-forwarded-for", new String[]{forwarded});
        template.header("Proxy-Client-IP", new String[]{IP});
        System.out.println(token + "ddd" + userAgent + "dddd" + contentType);
    }
}

