package com.exam.listener;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

//监听支付宝返回信息
@RestController
public class OrderPayedListener {
    @RequestMapping("/pay/notify")
    public String handleAlipayed(HttpServletRequest request){
        Map<String,String[]> map=request.getParameterMap();
        System.out.println("支付宝通知到位了数据时："+map);
        return "success";
    }
}
