package com.exam.service.impl;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.exam.config.AlipayConfig;
import com.exam.pojo.EnterpriseOrders;
import com.exam.service.AlipayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class  AlipayServiceImpl implements AlipayService {
    @Override
    public String aliPay(EnterpriseOrders eo) throws AlipayApiException {
        //获得初始化的AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient(AlipayConfig.gatewayUrl, AlipayConfig.app_id, AlipayConfig.merchant_private_key, "json", AlipayConfig.charset, AlipayConfig.alipay_public_key, AlipayConfig.sign_type);
        //设置请求参数
        AlipayTradePagePayRequest aliPayRequest = new AlipayTradePagePayRequest();
        aliPayRequest.setReturnUrl(AlipayConfig.return_url);
        aliPayRequest.setNotifyUrl(AlipayConfig.notify_url);

        aliPayRequest.setBizContent("{\"out_trade_no\":\"" + eo.getEnterpriseOrdersNumber() + "\","
                + "\"total_amount\":\"" + eo.getEnterpriseOrdersPrice() + "\","
                + "\"subject\":\"" + eo.getExamination().getExamName() + "\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");
        //请求
        String result=alipayClient.pageExecute(aliPayRequest).getBody();
        return result;
    }
}
