package com.exam.service;

import com.alipay.api.AlipayApiException;
import com.exam.pojo.EnterpriseOrders;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface AlipayService {
    String  aliPay(EnterpriseOrders eo) throws AlipayApiException;
}
