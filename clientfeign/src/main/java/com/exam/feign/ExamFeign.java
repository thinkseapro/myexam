package com.exam.feign;

import com.exam.pojo.Examination;
import com.exam.pojo.Subject;
import com.exam.vo.ExaminationExamVo;
import com.exam.vo.ExaminationVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 考试feign
 * @author loop
 */
@FeignClient(value = "exam-exercise")
public interface ExamFeign {

    /**
     * 考试考试
     * @param uid 用户编号
     * @param eeid 详情编号
     * @return
     */
    @ResponseBody
    @RequestMapping("/exam/startExam")
    ExaminationExamVo startExam(
             @RequestParam(value = "uid", required = false)  String uid,
             @RequestParam(value = "eeid", required = false)  Integer eeid);

    /**
     * 预加载试卷
     *
     * @return 返回当天需要考试的试卷
     */
    @RequestMapping("/exam/readyExam")
     List<ExaminationExamVo> readyExam();

    /**
     * 查看考试详情
     *
     * @return
     */
    @RequestMapping("/exam/examDeatils")
    ExaminationExamVo examDeatils(
             @RequestParam(value = "userId", required = false) String userId ,
             @RequestParam(value = "deatilsId", required = false)  String deatilsId);

    /**
     * 提交试卷
     * 修改考试记录
     *
     * @return
     */
    @RequestMapping("/exam/commitExam")
    @ResponseBody
    boolean commitExam(@RequestBody ExaminationExamVo obj);

    /**
     * 提交练习
     *
     * @param obj
     * @return
     */
    @RequestMapping("/exam/commitExercise")
    @ResponseBody
    boolean commitExercise(@RequestBody ExaminationExamVo obj);

    /**
     * 随机试题生成类
     * 开始即生成答题记录
     * 退出不提交还可进入
     * @return 类似试卷的答题集合
     */
    @ResponseBody
    @RequestMapping("/exam/randomTopic")
    ExaminationExamVo randomTopic(
            @RequestParam(value = "userId", required = false) String userId,
            @RequestParam(value = "subId", required = false)  Integer subId);
    /**
     * 查看练习详情
     *
     * @return
     */
    @ResponseBody
    @RequestMapping("/exam/exerciseDeatils")
    ExaminationExamVo exerciseDeatils(
            @RequestParam(value = "userId", required = false)  String userId ,
            @RequestParam(value = "deatilsId", required = false)  String deatilsId);

    //根据试卷id查询试卷
    @RequestMapping("/exam/getOneByEid")
    @ResponseBody
    Examination getOneByEid(@RequestParam(value="eid") int eid);
    /**
     * 查询所有科目
     * @return
     */
    @RequestMapping("/exam/selSubject")
    List<Subject> selSubject();
}
