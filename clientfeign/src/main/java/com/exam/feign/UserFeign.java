package com.exam.feign;

import com.exam.constant.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@FeignClient(value = "exam-users")
public interface UserFeign {

    @RequestMapping("/user/yz")
    Result yz(HttpServletRequest request);

    @RequestMapping("/user/login")
    Result login(@RequestParam("username") String username, @RequestParam("password") String password);

    @RequestMapping("/user/exit")
    Result exit(@RequestParam("username") String username);

    @RequestMapping("/user/updatePassword")
    Result updatePassword(@RequestParam("password") String password);

    @RequestMapping("/user/ismail")
    Result isEmail(@RequestParam("email") String email);

    @RequestMapping("/user/mail/resetPassword")
    Result resetPassword(@RequestParam("mail") String mail);

    @RequestMapping("/user/mail/PasswordVerify/{tokens}")
    Result mailVerify(@PathVariable("tokens") String tokens);

    @RequestMapping("/user/PhoneCode")
    Result PhoneCode(@RequestParam("phone") String phone);

    @RequestMapping("/user/receiveCode")
    Result receiveCode(@RequestParam("phone") String phone,@RequestParam("code") String code);

    @RequestMapping("/user/getNickname")
    Result getTokenNickname();

    @RequestMapping("/user/ver/findUserByid")
    Result getUser();

    @RequestMapping(value = "/user/ver/uploadAvatar")
    Result uploadAvatar(@RequestParam("file") String file);

    @RequestMapping(value = "/user/ver/sentEmailCode")
    Result sentEmailCode(@RequestParam("email") String email);

    @RequestMapping(value = "/user/ver/VerifyEmailCode")
    Result VerifyEmailCode(@RequestParam("email") String email,@RequestParam("newemail") String newemail,@RequestParam("code") String code);

    @RequestMapping(value = "/user/ver/sentPhoneCode")
    Result sentPhoneCode(@RequestParam("phone") String phone);

    @RequestMapping(value = "/user/ver/VerifyPhoneCode")
    Result VerifyPhoneCode(@RequestParam("phone") String phone,@RequestParam("newphone") String newphone,@RequestParam("code") String code);

}
