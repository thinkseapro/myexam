package com.exam.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.pojo.CountExam;
import com.exam.pojo.ExerciseDetails;
import com.exam.pojo.Subject;
import com.exam.vo.MyScoreDataVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;


/**
 * (查看成绩<练习,考试>)
 */
@FeignClient(value = "exam-achievments")
public interface ScoreFeign {
    @RequestMapping("/achievements/scoreList")
    MyScoreDataVo getScoreDetails(@RequestParam("page") Integer page, @RequestParam("limit") Integer limit, @RequestBody ExerciseDetails ed);

    @RequestMapping("/achievements/subjectList")
    List<Subject> getSubList();

    @RequestMapping("/achievements/examList")
    MyScoreDataVo getExamList(@RequestParam("page") Integer page, @RequestParam("limit") Integer limit, @RequestBody ExerciseDetails ed);

    @RequestMapping("/achievements/getCountExam")
    CountExam getCountExam(@RequestBody ExerciseDetails ed);

    @RequestMapping("/achievements/indexExam")
    Page<ExerciseDetails> indexexam(@RequestParam(value = "uid") String uid);

    @RequestMapping("/achievements/insertExerciseDetails")
    boolean insertExerciseDetails(@RequestBody ExerciseDetails exerciseDetails);
}
