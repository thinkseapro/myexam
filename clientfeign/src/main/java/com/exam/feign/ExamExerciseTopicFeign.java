package com.exam.feign;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.vo.ExamExerciseTopicErrorVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * <p>
 * 选项内容/错题信息
 * </p>
 *
 * @author loop
 * @since 2020-12-10
 */
@FeignClient(value = "exam-exercise")
public interface ExamExerciseTopicFeign {

    @RequestMapping("/exam-exercise-topic/getExerciseTopicError")
    Page<ExamExerciseTopicErrorVo> getExerciseTopicError(@RequestBody ExamExerciseTopicErrorVo errorVo);
}
