package com.exam.feign;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.pojo.EnterpriseDetails;
import com.exam.pojo.EnterpriseOrders;
import com.exam.pojo.UserEnterprise;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "exam-payment")
public interface EnterpriseFeign {

    //查看企业试卷
    @RequestMapping("/payment/getenterpriseList/{current}/{size}")
    Page<EnterpriseDetails> getenterpriseList(@RequestBody EnterpriseDetails ed, @PathVariable(value = "current") int current, @PathVariable(value = "size") int size);
    //新增企业试卷订单
    @RequestMapping("/payment/insertEnterOrder")
    boolean insertEnterOrder(@RequestBody EnterpriseOrders enterpriseOrders);

    //新增用户-企业试卷中间表
    @RequestMapping("/payment/insertUserEnterprise")
    boolean insertUserEnterprise(@RequestBody UserEnterprise userEnterprise);

    //修改企业试卷详情表
    @RequestMapping("/payment/upEnterDetails")
    boolean updateEnterDetails(@RequestBody EnterpriseDetails enterpriseDetails);

    //查看订单
    @RequestMapping("/payment/enterOrderPage/{enterTypeid}")
    Page<EnterpriseOrders> enterOrderPage(@PathVariable(value = "enterTypeid") Integer enterTypeid);

    //显示首页企业试卷列表
    @RequestMapping("/payment/indexEnterpriseDetails")
    Page<EnterpriseDetails> indexEnterpriseDetails(@RequestBody EnterpriseDetails ed);
    //根据订单号查看订单
    @RequestMapping("/payment/getOneByOrdersNumber")
    EnterpriseOrders getOneByOrdersNumber(@RequestParam(value="oid") long oid);
    //修改订单状态为已支付
    @RequestMapping("/payment/updateEnterOrders")
    boolean updateEnterOrders(@RequestParam(value="oid") long oid);
    //根据企业详情id查询试卷
    @RequestMapping("/payment/getOneByDetailsId")
    EnterpriseDetails getOneByDetailsId(@RequestParam(value="did") int did);

}
