package com.exam.feign;

import com.exam.constant.ExamCode;
import com.exam.constant.Result;
import com.exam.pojo.ExamTopicTick;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value = "exam-exercise")
public interface ExamTopicTickFeign {

    @RequestMapping("/exam-topic-tick/addTopicTick")
    Result addTopicTick(@RequestBody ExamTopicTick examTopicTick);

}
