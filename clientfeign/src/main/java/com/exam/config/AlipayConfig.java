package com.exam.config;

import java.io.FileWriter;
import java.io.IOException;
//沙箱支付
public class AlipayConfig {

	// 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
	public static String app_id = "2021000116677298";

	// 商户私钥，您的PKCS8格式RSA2私钥
	public static String merchant_private_key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDSJFM7LP8FaFms4pEd7MXeb0uUdzz4CDI8wUtTn3hFk5gRLyaEe3//LbEEZr0FxcCbJCHLoyZpamIzGAFofkNYz9O4n5NrA2UtyBRnY6Iq/dWglaLv4B0y/J074lkYzG7MU3QNNKsul3vX0BlOX44nqM6Eurlb0GPYq8A+WdjAcAYdc6/CXpXSjZpHXrlFRiasMu7QwKecntoVndjixPpH6Ux5BanRdSl+HR086lVs2C+SaAfFozgXLPtKd5QLbPZDgje/Mj0qJQs+2b2gXLTFaCrqOdCu1JY8x6gCoWR6U3uTd5+4TumKJWdtjNUys3tQe43BnEuPOqPvGeLiGKcXAgMBAAECggEBALCeSz4PQYAgBk7iVimamWusiw+LcDjfWqnppA/yljmUJVk8EdmDmz6z/nW+i9xQKuO56JWo2F39Re+/m19hz+TTOBWSOWrV/bIO2edTvDb6BRTk739BFAmIe/aa9Vt8ZxtggMegnseYFkR66OoQMgBvRrQT5Dqg2WqAYCGHjj+d4TQgWzmObK6nj6jhezyxjHtryStx4VpBI4oqMasC05/Yg5og7G8DSNN/j01lR6qstRT3n6ku0Q5CIyYNfbVn27f0mDp3b9fVvFFRD+Kc0VLSq1iRYwbx/uqlyOPsCui2LA/+Aiuvqbk89zzxsC02I1t/ZhW4MvyivVG6XUWcmjECgYEA92Cshnbvcme/PmMLjNdPHwEy3Whz9ADa212mb5QlGDaViKb8RA9tkViBJ64dtd7d4GwPIYGiFlXM3o624lNagh5sFb3uym4KFXui65gNnCPNYFQY2RZ2b4GQPktWF0u4SFeYQ4duG9D2ZHIvqLh376wGP2wT6ZcaodDaW3LHaQMCgYEA2XdmgNGorxw5GpwDERKnfDUDvwSfOJEcaYYp8OZ/Ppue/Aj4Fk/Wy4vAsxVKERiYiKRvDm+WaYTUfcZWRjJonYJv4KBrAdtbm46FuNpqRpkNljFYsagEJW1PUotZ60WY/lo/vdt7zeoVKM369uLi22WvGw5xBT7Tam+2imGsK10CgYEAu+aepXRp5MiRJ14sQ8q3uqE6dZsFmG2dz8qOxWoN0xqC8s2ECF5/p0tGr1E+GAs1FWZ/B8Bj0ftSJirZF4ig0v0wtEMUwoZa+n5y/ej9TYCUW3sFdL/vLjuHyQmIVyGY1LpUWdbg+Bf9T8a8FLgpwc8036/EYEX6YLc3ZQcGGOkCgYB7vWgTBrmQImQP0xQfo9ivYU8nYw7iVhevp8q3q8BdtJlLWiMoNvfGzFeyzvtdLPymS8JxmWch1ZY/lxrCSK6kaXMVgtJAtTu0EO3gGE/+qdpESwHn3O23CjLniil5KRn8KQg6FOima3rsw//4eXOSWZLAt3/x9yS/XJ+msBsSdQKBgGDplx8wNoa0ItoRJJSNAYcEwZYi7VPg8Q9JFvJL3qZ4iZvjFFPwCOlgA3BwwpV34lLzY1Ggc3tbohNzBbvXMOrfyAPCO3AKr6MaekqTihLlwT9xbBzYxBsWjHEEnaIsMoF0vHJ+b13ISMWDEGMiVkeJmgf8PQemFE3oeIbVNT5n";
	// 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
	public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjXwcC6PTHaTYVOYndZ3t/6ONyRPkeOCfJFlocfUH5kLpRDK4jfz/A8Ldih14B77YqEzlnxsJjBJGHPd2UsO5jZpAKLt6rCYzoGIw5vSx4V0UKY7yZH3x5oGbOaeDPvXs/kMoxv6NY8fIWwkuhfAwpDzqVLh+wRYVIBjIxyRi2of00n6r5+Yza20VH1b2BZcPUKGuCoGWJihr6MPCvjCE4E46bJuIWaQzxFHdqv1GZ6p6hefbTyZXpLTVGr3DajLF/LoJxHtB+M1iEgank02TPy2tSVGPybGnb7AkCd8PD6G9nsH5yKqdUG8XSLE76A1dH10suEQ0p6obgA/XxiXVbQIDAQAB";

	// 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String notify_url = "http://jd2mb9sl6c.52http.tech/clientfeign/pay/";
    //http://jd2mb9sl6c.52http.tech/clientfeign/pay/
	// 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String return_url = "http://jd2mb9sl6c.52http.tech/clientfeign/pay/";

	// 签名方式
	public static String sign_type = "RSA2";

	// 字符编码格式
	public static String charset = "utf-8";

	// 支付宝网关
	public static String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

	// 支付宝网关
	public static String log_path = "C:\\";

	/**
	 * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
	 * @param sWord 要写入日志里的文本内容
	 */
	public static void logResult(String sWord) {
		FileWriter writer = null;
		try {
			writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
			writer.write(sWord);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}

